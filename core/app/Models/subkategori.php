<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class subkategori extends Model
{
    use HasFactory;
    protected $table = 'subkategori';
    protected $fillable = [
        'id_kategori',
        'nama_subkategori',
        'foto',
        'created_at',
        'updated_at',
    ];

    public function select_subkategori()
    {
        return $this->hasMany(portofolio::class);
    }
}
