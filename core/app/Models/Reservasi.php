<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservasi extends Model
{
    use HasFactory;
    protected $table = 'reservasi';
    protected $fillable = [
        'nama',
        'email',
        'no_telp',
        'waktu',
        'tanggal',
        'created_at',
        'updated_at',
    ];
}
