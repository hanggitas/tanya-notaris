<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class detail extends Model
{
    use HasFactory;
    protected $table = 'detail';
    protected $fillable =[
        'id_order',
        'id_subkategori',
        'harga',
        'created_at',
        'updated_at'
    ];
}
