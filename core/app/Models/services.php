<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class services extends Model
{
    use HasFactory;
    protected $table = 'services';
    protected $fillable = [
        'jenis_paket',
        'detail_paket',
        'harga',
        'foto',
        'created_at',
        'updated_at'
    ];
}
