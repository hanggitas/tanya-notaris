<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    use HasFactory;
    protected $table = 'order';
    protected $fillable = [
        'tipe_order',
        'id_users',
        'bukti_bayar_dp',
        'bukti_bayar_lunas',
        'id_metode_pembayaran',
        'id_service_subkategori',
        'bukti_bayar_dp',
        'bukti_bayar_lunas',
        'status_pembayaran',
        'date_time',
        'total_harga',
        'total_dibayar',
        'sisa_bayar',
        'created_at',
        'updated_at'
    ];
}
