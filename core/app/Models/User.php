<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    use HasFactory;
    protected $table = 'users';
    protected $fillable = [
        'name',
        'email',
        'role',
        'password',
        'foto',
        'email_verified_at',
        'no_hp_user',
        'alamat_user',
        'remember_token',
        'created_at',
        'updated_at',
    ];
}


// class user extends Model
// {

// }
