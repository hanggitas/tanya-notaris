<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class progres extends Model
{
    use HasFactory;
    protected $table = 'progres';
    protected $fillable = [
        'id_users',
        'id_order',
        'persentase_progres',
        'start_date',
        'end_date',
        'created_at',
        'updated_at'
    ];
}
