<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class goal extends Model
{
    use HasFactory;
    protected $table = 'goal';
    protected $fillable = [
        'target',
        'created_at',
        'updated_at'
    ];
}
