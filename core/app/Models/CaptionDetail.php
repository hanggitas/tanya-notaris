<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CaptionDetail extends Model
{
    use HasFactory;
    protected $table = 'caption_detail';
    protected $fillable = [
      'caption1',
      'caption2',
      'caption3',
      'caption4',
      'caption5',
        'created_at',
        'updated_at',
    ];
}
