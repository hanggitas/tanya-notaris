<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class detail_task extends Model
{
    use HasFactory;
    protected $table = 'detail_task';
    protected $fillable = [
        'id_order',
        'id_task',
        'status',
        'created_at',
        'updated_at',
    ];
}
