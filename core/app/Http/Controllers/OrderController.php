<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\order;
use App\Models\kategori;
use App\Models\metode_pembayaran;
use App\Models\service_subkategori;
use App\Models\services;
use App\Models\subkategori;
use App\Models\User;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function order(Request $request)
    {
        //$orders = order::all();
        $pilihan_kategori = kategori::all();
        //dd($pilihan_kategori);
        $pilihan_subkategori = subkategori::all();
        // $detail =  service_subkategori::leftjoin('kategori', 'service_subkategori.id_kategori', '=', 'kategori.id')
        //     ->leftjoin('subkategori', 'service_subkategori.id_subkategori', '=', 'subkategori.id')
        //     ->leftjoin('services', 'service_subkategori.id_services', '=', 'services.id')
        //     ->select('kategori.nama_kategori', 'subkategori.nama_subkategori', 'services.jenis_paket')
        //     ->groupBy('service_subkategori.id')
        //     ->where('service_subkategori.id')
        //     ->get();
        $orders = order::select('order.*', 'metode_pembayaran.metode_pembayaran', 'users.name', 'service_subkategori.id as sersub_id')
            ->leftjoin('metode_pembayaran', 'order.id_metode_pembayaran', '=', 'metode_pembayaran.id')
            ->leftjoin('users', 'order.id_users', '=', 'users.id')
            ->leftjoin('service_subkategori', 'order.id_service_subkategori', '=', 'service_subkategori.id')
            ->paginate(5);
        return view('main.order', compact('orders', 'pilihan_kategori', 'pilihan_subkategori'));
    }

    public function formorder()
    {
        $pilihan_kategori = kategori::all();
        return view('input_form.tambah_data_order', compact('pilihan_kategori'));
    }

    public function postorder(Request $request)
    {
        $data = $request->all();
        //dd($data);
        $post = [
            'id_users' => $data['id_users'],
            'id_metode_pembayaran' => $data['id_metode_pembayaran'],
            'id_service_subkategori' => $data['id_service_subkategori'],
            'bukti_bayar_dp' => $data['bukti_bayar_dp'],
            'bukti_bayar_lunas' => $data['bukti_bayar_lunas'],
            'status_pembayaran' => $data['status_pembayaran'],
            'date_time' => $data['date_time'],
            'total_harga' => $data['total_harga'],
            'total_dibayar' => $data['total_dibayar'],
            'sisa_bayar' => $data['sisa_bayar']
        ];

        order::create($post);
        return redirect()->back();
    }

    public function postcustom(Request $request)
    {

        $data = $request->all();
        //dd($data);
        $post = [
            'jenis_paket' => $data['jenis_paket'],
            'detail_paket' => $data['detail_paket'],
            'harga' => $data['harga']
        ];
        $postServis = services::create($post);

        $postKategori = [
            'id_services' => $postServis->id,
            'id_kategori' => $data['id_kategori'],
            'id_subkategori' => $data['id_subkategori']
        ];
        $postSersub = service_subkategori::create($postKategori);

        $apaaja = order::where('id', $request->input('id_order'))->update([
            'id_service_subkategori' => $postSersub->id,
            'total_harga' => $data['harga'],
            'total_bayar_dp' => $data['harga'] / 2
        ]);
        return redirect('/order');
    }

    public function edit(Request $request)
    {
        $order = order::find($request->route('id'));
        //dd($order);
        $pilihan_medpem = metode_pembayaran::get();
        $pilihan_user = User::get();
        $orders = order::leftjoin('users', 'order.id_users', '=', 'users.id')
            ->leftjoin('metode_pembayaran', 'order.id_metode_pembayaran', '=', 'metode_pembayaran.id')
            ->get(['order.*', 'users.name', 'metode_pembayaran.metode_pembayaran']);
        return view('edit.edit_order', compact('order', 'orders', 'pilihan_medpem', 'pilihan_user'));
    }

    public function updateorder(Request $request)
    {
        //dd($request->input('total_dibayar') / 2);
        $update = order::find($request->route('id'));
        $update->id_metode_pembayaran = $request->input('id_metode_pembayaran');
        $update->total_bayar_lunas = $request->input('total_bayar_lunas');
        $update->sisa_bayar = $request->input('sisa_bayar');
        $update->status_pembayaran = $request->input('status_pembayaran');
        $update->update();
        //dd($update);
        return redirect('/order');
        //return $request;
    }

    public function delete(Request $request)
    {
        $order = order::find($request->route('id'));
        $order->delete();
        return redirect()->route('order')->with('status', 'Data Berhasil Dihapus!');
    }
}
