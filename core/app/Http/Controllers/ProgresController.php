<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\progres;
use Illuminate\Http\Request;

class ProgresController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function progres()
    {
        $progress = progres::all();
        return view('main.progres', compact('progress'));
    }

    public function formprogres()
    {
        return view('input_form.tambah_data_progres');
    }

    public function postprogres(Request $request)
    {
        $data = $request->all();
        //dd($data);
        $post = [
            'id_users' => $data['id_users'],
            'id_order' => $data['id_order'],
            'persentase_progres' => $data['persentase_progres'],
            'start_date' => $data['start_date'],
            'end_date' => $data['end_date'],
            //dd($data);
        ];

        progres::create($post);
        return redirect()->back();
    }

    public function edit($id)
    {
        $progres = progres::find($id);
        return view('input_form.ubah_data_progres', compact('progres'));
    }

    public function update(Request $request, $id)
    {
        $progres = progres::find($id);
        $progres->id_user = $request->input('id_user');
        $progres->id_detail = $request->input('id_detail');
        $progres->persentase_progres = $request->input('persentase_progres');
        $progres->start_date = $request->input('start_date');
        $progres->end_date = $request->input('end_date');
        $progres->update();

        if ($progres) {
            //redirect dengan pesan sukses
            return redirect()->route('progres')->with(['success' => 'Data Berhasil Diupdate!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('progres')->with(['error' => 'Data Gagal Diupdate!']);
        }
    }

    public function delete($id)
    {
        $progres = progres::find($id);
        $progres->delete();
        return redirect()->route('progres')->with('status', 'Data Berhasil Dihapus!');
    }
}
