<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\portofolio;
use App\Models\User;
use App\Models\service_subkategori;
use App\Models\users;
use App\Models\kategori;
use App\Models\preview;
use Illuminate\Http\Request;

class PortofolioController extends Controller
{
    //
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function portofolio()
    {
        //$portofolio = portofolio::all();
        // $pilihan_kategori = order::join('service_subkategori as ss', 'order.id_service_subkategori', '=', 'ss.id')
        //     ->join('kategori', 'order.id_service_subkategori', '=', 'kategori.id')
        //     ->select('order.*', 'kategori.nama_kategori')
        //     ->get();

        // $pilihan_subkategori = order::join('service_subkategori as ss', 'order.id_service_subkategori', '=', 'ss.id', 'inner')
        //     ->join('subkategori', 'order.id_service_subkategori', '=', 'subkategori.id', 'inner')
        //     ->get('subkategori.nama_subkategori');
        // $left_join = kategori::all();
        $left_join = portofolio::select('portofolio.*', 'kategori.nama_kategori', 'users.name')
            ->leftjoin('kategori', 'portofolio.id_kategori', '=', 'kategori.id')
            ->leftjoin('users', 'portofolio.id_users', '=', 'users.id')
            // ->leftjoin('preview', 'portofolio.id_preview', '=', 'preview.id')
            ->paginate(5);
        //dd($left_join);
        return view('main.portofolio', compact('left_join'));
    }

    public function tambah_data_portofolio()
    {
        // $left_join = portofolio::leftjoin('users', 'portofolio.id_users', '=', 'users.id')
        //     ->get(['portofolio.*', 'users.name']);
        $pilihan_kategori = kategori::all();
        $pilihan_user = User::all();
        // $pilihan_link = preview::all();

        return view('input_form.tambah_data_portofolio', compact('pilihan_kategori', 'pilihan_user'));
    }

    public function postportofolio(Request $request)
    {
        $this->validate($request, [
            'id_users' => 'required',
            'id_kategori' => 'required',
            'projek' => 'required'
        ]);

        $data = $request->all();
        $post = [
            'id_users' => $data['id_users'],
            'id_kategori' => $data['id_kategori'],
            // 'id_preview' => $data['id_preview'],
            'projek' => $data['projek']
        ];
        $buat = portofolio::create($post);
        if ($request->hasFile('projek')) {
            $request->file('projek')->move('public/app-assets/images/docs/', $request->file('projek')->getClientOriginalName());
            $buat->projek = $request->file('projek')->getClientOriginalName();
            $buat->save();
        }
        //dd($data);

        return redirect('/portofolio');
    }

    public function edit(Request $request, $id)
    {
        $portofolio = portofolio::find($id);
        $pilihan_kategori = kategori::all();
        $pilihan_user = User::all();
        // $pilihan_link = preview::all();

        //dd($pilihan_user);
        return view('input_form.ubah_data_portofolio', compact('portofolio', 'pilihan_kategori', 'pilihan_user'));
    }

    public function update(Request $request, $id)
    {


        $portofolio = portofolio::find($request->route('id'));
        //dd($portofolio);
        $portofolio->id_users = $request->input('id_users');
        $portofolio->id_kategori = $request->input('id_kategori');
        // $portofolio->id_preview = $request->input('id_preview');
        if ($request->hasFile('projek')) {
            $file = $request->file('projek');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('public/app-assets/images/docs/', $filename);
            $portofolio->projek = $filename;
        }
        $portofolio->update();
        //dd($portofolio);

        // if ($portofolio) {
        //     //redirect dengan pesan sukses
        //     return redirect()->route('portofolio')->with(['success' => 'Data Berhasil Diupdate!']);
        // } else {
        //     //redirect dengan pesan error
        //     return redirect()->route('portofolio')->with(['error' => 'Data Gagal Diupdate!']);
        // }

        return redirect('/portofolio');
    }

    public function delete($id)
    {
        $portofolio = portofolio::find($id);
        $portofolio->delete();
        return redirect()->route('portofolio')->with('status', 'Data Berhasil Dihapus!');
    }
}
