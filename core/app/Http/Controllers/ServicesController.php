<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\services;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ServicesController extends Controller
{
    //
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function servis()
    {
        $services = services::paginate(5);
        return view('main.service', compact('services'));
    }

    public function tambah_data_service()
    {
        return view('input_form.tambah_data_service');
    }

    public function postservice(Request $request)
    {
        $this->validate($request, [
            'jenis_paket' => 'required',
            'detail_paket' => 'required',
            'harga' => 'required',
            'foto' => 'required'
        ]);

        // $validator = Validator::make($request->all(), 
        // ['foto' => 'mimes:jpg,jpeg,png|max:10000',
        // // 'token' => 'required|string'
        // ]);

        // if($validator->fails())
        // {
        //    return redirect()->back()->withErrors($validator)->withInput(); 
        // }


        // $data = $request->all();
        // $post = [
        //     'jenis_paket' => $data['jenis_paket'],
        //     'detail_paket' => $data['detail_paket'],
        //     'harga' => $data['harga'],
        //     'foto' => $data['foto']
        // ];
        // $buat = services::create($post);
        // if ($request->hasFile('foto')) {
        //     $request->file('foto')->move('public/app-assets/images/illustration/', $request->file('foto')->getClientOriginalName());
        //     $buat->foto = $request->file('foto')->getClientOriginalName();
        //     $buat->save();
        // }
        $data = services::create($request->all());
        if ($request->hasFile('foto')) {
            $request->file('foto')->move('public/app-assets/images/illustration/', $request->file('foto')->getClientOriginalName());
            $data->foto = $request->file('foto')->getClientOriginalName();
            $data->save();
        }
        return redirect()->route('servis');
    }

    public function edit($id)
    {
        $service = services::find($id);
        return view('input_form.ubah_data_service', compact('service'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'jenis_paket' => 'required',
            'detail_paket' => 'required',
            'harga' => 'required'
        ]);

        $service = services::find($id);
        $service->jenis_paket = $request->input('jenis_paket');
        $service->detail_paket = $request->input('detail_paket');
        $service->harga = $request->input('harga');
        if ($request->hasFile('foto')) {
            $file = $request->file('foto');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('public/app-assets/images/illustration/', $filename);
            $service->foto = $filename;
        }
        $service->update();
        return redirect()->route('servis');

        if ($service) {
            //redirect dengan pesan sukses
            return redirect()->route('servis')->with(['success' => 'Data Berhasil Diupdate!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('servis')->with(['error' => 'Data Gagal Diupdate!']);
        }
    }

    public function delete($id)
    {
        $service = services::find($id);
        $service->delete();
        return redirect()->route('servis')->with('status', 'Data Berhasil Dihapus!');
    }
}
