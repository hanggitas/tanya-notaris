<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\pekerjaan;
use App\Models\order;
use App\Models\goal;
use App\Models\User;

class PekerjaanController extends Controller
{
    public function pekerjaan()
    {
        //$data_pekerjaan = pekerjaan::all();
        //dd($data_pekerjaan);

        $data_pekerjaan = pekerjaan::leftjoin('users', 'pekerjaan.id', '=', 'users.id')
            ->select(['pekerjaan.*', 'users.name'])->paginate(5);
        return view('main.pekerjaan', compact('data_pekerjaan'));
    }

    public function formpekerjaan()
    {
        $pilihan_order = order::all();
        return view('input_form.tambah_data_pekerjaan', compact('pilihan_order'));
    }

    public function postpekerjaan(Request $request)
    {
        $data = $request->all();
        $post = [
            'id_users' => $data['id_users'],
            'pre_produksi' => $data['pre_produksi'],
            'produksi' => $data['produksi'],
            'post_produksi' => $data['post_produksi'],
            'selesai' => $data['selesai'],
            'link_project' => $data['link_project'],
            'size_project' => $data['size_project'],
            'diunggah' => $data['diunggah'],
        ];
        pekerjaan::create($post);
        return redirect('/pekerjaan');
    }

    public function edittpekerjaan(Request $request, $id)
    {
        $data_pekerjaan = pekerjaan::find($id);
        //dd($data_pekerjaan);
        // $pilihan_user = User::get();
        $user = pekerjaan::leftjoin('users', 'pekerjaan.id', '=', 'users.id')
            ->select(['pekerjaan.*', 'users.name']);
        return view('edit.edit_pekerjaan', compact('data_pekerjaan', 'user'));
        //['data_pekerjaan' => $data_pekerjaan, 'pilihan_user' => $pilihan_user]
    }

    public function updatepekerjaan(Request $request)
    {
        $updatepekerjaan = pekerjaan::find($request->route('id'));
        $updatepekerjaan->update($request->all());
        return redirect('/pekerjaan');
    }

    public function hapuspekerjaan(Request $request)
    {
        $deletepekerjaan = pekerjaan::find($request->route('id'));
        $deletepekerjaan->delete();
        return redirect('/pekerjaan');
    }

    public function target()
    {
        //$data_pekerjaan = pekerjaan::all();
        //dd($data_pekerjaan);

        $data_target = goal::get();
        return view('main.target', compact('data_target'));
    }

    public function posttarget(Request $request)
    {
        $data = $request->all();
        //dd($data);
        goal::create($data);
        return redirect('/target');
    }

    public function edittarget(Request $request, $id)
    {
        $data_target = goal::find($id);
        //dd($data_pekerjaan);

        return view('edit.edit_target', compact('data_target'));
        //['data_pekerjaan' => $data_pekerjaan, 'pilihan_user' => $pilihan_user]
    }

    public function updatetarget(Request $request)
    {
        $updatetarget = goal::find($request->route('id'));
        $updatetarget->update($request->all());
        return redirect('/target');
    }

    public function hapustarget(Request $request)
    {
        $deletetarget = goal::find($request->route('id'));
        $deletetarget->delete();
        return redirect('/target');
    }
}
