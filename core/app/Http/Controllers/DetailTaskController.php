<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\detail_task;
use App\Models\task;
use Illuminate\Http\Request;

class DetailTaskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function detailtask()
    {
        $left_join = detail_task::leftjoin('task', 'detail_task.id_task', '=', 'task.id')
            ->get(['detail_task.*', 'task.deskripsi']);

        return view('main.detailtask', compact('left_join'));
        //$data_detail_task = DB::table('detail_task')->get();
        //return view('main.detailtask', ['data_detail_task' => $data_detail_task]);
    }

    public function formdetailtask()
    {
        $pilihan_task = task::all();
        return view('input_form.tambah_data_detail_task', compact('pilihan_task'));
    }

    public function postdetailtask(Request $request)
    {
        $data = $request->all();
        //dd($data);
        $post = [
            'id_order' => $data['id_order'],
            'id_task' => $data['id_task'],
            'status' => $data['status'],
        ];
        detail_task::create($post);
        return redirect('/detailtask'); //->back();
    }

    public function editdetailtask(Request $request)
    {
        $pilihan_task = task::all();
        //dd($pilihan_task);
        $data_detail_task = detail_task::find($request->route('id'));
        //dd($request->route('id'));
        //return view('edit.edit_detailtask', ['data_detail_task' => $data_detail_task]);
        //dd($data_detail_task);
        return view('edit.edit_detailtask', ['data_detail_task' => $data_detail_task, 'pilihan_task' => $pilihan_task]);
    }

    public function updatedetailtask(Request $request)
    {
        $updatedetailtask = detail_task::find($request->route('id'));
        //Cara 1
        //$updatedetailtask->id_detail = $request->id_detail;
        //$updatedetailtask->id_task = $request->id_task;
        //$updatedetailtask->status = $request->status;
        //dd($request);
        //$updatedetailtask->save();
        //Cara2
        $updatedetailtask->update($request->all());
        return redirect('/detailtask');
    }

    public function deletedetailtask(Request $request)
    {
        $deletedetailtask = detail_task::find($request->route('id'));
        $deletedetailtask->delete();

        return redirect('/detailtask');
    }
}
