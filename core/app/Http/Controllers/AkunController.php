<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;

class AkunController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function akun()
    {
        // $user = User::select('name', 'email', 'no_hp_user', 'alamat_user')
        //     ->where('id', Auth::admin()->id())
        //     ->first();, compact('user')

        return view('main.akun');
    }

    public function task()
    {
        $data_task = task::get();
        //return view('main.task', compact('task'));
        //$data_task = DB::table('task')->get();
        return view('main.task', ['data_task' => $data_task]);
    }

    public function formtask()
    {
        return view('input_form.tambah_data_task');
    }

    public function posttask(Request $request)
    {
        $data = $request->all();
        $post = [
            'deskripsi' => $data['deskripsi']
        ];

        task::create($post);
        return redirect('/task'); //->back();
        //dd($data);
    }

    public function edittask(Request $request)
    {
        $data_task = task::find($request->route('id'));
        //dd($request->route('id'));
        return view('edit.edit_task', ['data_task' => $data_task]);
    }

    public function update(Request $request)
    {
        $update = User::find($request->route('id'));
        $update->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'alamat_user' => $request->input('alamat_user'),
            'no_hp_user' => $request->input('no_hp_user'),
        ]);
        // $updatetask = task::find($request->route('id'));
        //Cara 1
        //$updatetask->deskripsi = $request->deskripsi;
        //$updatetask->save();
        //Cara 2
        //$updatetask->update($request->all());
        return redirect('/akun');

        //return $request;
    }

    public function deletetask(Request $request)
    {
        $deletetask = task::find($request->route('id'));
        $deletetask->delete();

        return redirect('/task');
    }
}
