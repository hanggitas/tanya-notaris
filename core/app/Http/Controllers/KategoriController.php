<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class KategoriController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    //
    public function kategori()
    {
        $kategori = kategori::paginate(5);
        return view('main.kategori', compact('kategori'));
    }

    public function tambah_data_kategori()
    {
        return view('input_form.tambah_data_kategori');
    }

    public function postkategori(Request $request)
    {
        $this->validate($request, [
            'nama_kategori' => 'required',
            'foto' => 'required'
        ]);

        $validator = Validator::make(
            $request->all(),
            [
                'foto' => 'mimes:jpg,jpeg,png|max:10000',
                // 'token' => 'required|string'
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        // $data = $request->all();
        // $post = [
        //     'nama_kategori' => $data['nama_kategori'],
        //     'foto' => $data['foto']
        // ];
        // $buat = kategori::create($post);
        // if ($request->hasFile('foto')) {
        //     $request->file('foto')->move('public/app-assets/images/pages/eCommerce/', $request->file('foto')->getClientOriginalName());
        //     $buat->foto = $request->file('foto')->getClientOriginalName();
        //     $buat->save();
        // }
        $data = kategori::create($request->all());
        if ($request->hasFile('foto')) {
            $request->file('foto')->move('public/app-assets/images/pages/eCommerce/', $request->file('foto')->getClientOriginalName());
            $data->foto = $request->file('foto')->getClientOriginalName();
            $data->save();
        }
        return redirect()->route('kategori');
    }

    public function edit($id)
    {
        $kategori = kategori::find($id);
        return view('input_form.ubah_data_kategori', compact('kategori'));
    }

    public function update(Request $request, $id)
    {


        $kategori = kategori::find($id);
        // $kategori->nama_kategori = $request->input('nama_kategori');
        if ($request->hasFile('foto')) {
            $file = $request->file('foto');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('public/app-assets/images/pages/eCommerce/', $filename);
            $kategori->foto = $filename;
            $kategori->save();
        }
        $kategori->update([
            'nama_kategori' => $request->input('nama_kategori')
        ]);
        //dd($kategori);

        if ($kategori) {
            //redirect dengan pesan sukses
            return redirect()->route('kategori')->with(['success' => 'Data Berhasil Diupdate!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('kategori')->with(['error' => 'Data Gagal Diupdate!']);
        }
    }

    public function delete($id)
    {
        $kategori = kategori::find($id);
        $kategori->delete();
        return redirect()->route('kategori')->with('status', 'Data Berhasil Dihapus!');
    }
}
