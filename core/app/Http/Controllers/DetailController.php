<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\detail;
use Illuminate\Http\Request;

class DetailController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    public function detail()
    {
        $details = detail::all();
        return view('main.detail', compact('details'));
    }

    public function formdetail()
    {
        return view('input_form.tambah_data_detail');
    }

    public function postdetail(Request $request)
    {
        $data = $request->all();
        $post = [
            'id_order' => $data['id_order'],
            'id_subkategori' => $data['id_subkategori'],
            'harga' => $data['harga'],
        ];

        detail::create($post);
        return redirect()->back();
    }

    public function updatedetail(Request $request)
    {
        return $request;
    }

    public function delete($id)
    {
        $detail = detail::find($id);
        $detail->delete();
        return redirect()->route('detail')->with('status', 'Data Berhasil Dihapus!');
    }
}
