<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\service_subkategori;
use App\Models\subkategori;
use App\Models\kategori;
use App\Models\services;
use Illuminate\Http\Request;

class ServiceSubkategoriController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function service_subkategori(Request $request)
    {
        $left_join_services = service_subkategori::leftjoin('services', 'service_subkategori.id_services', '=', 'services.id')
            ->leftjoin('subkategori', 'service_subkategori.id_subkategori', '=', 'subkategori.id')
            ->leftjoin('kategori', 'service_subkategori.id_kategori', '=', 'kategori.id')
            ->select(['service_subkategori.*', 'services.jenis_paket', 'subkategori.nama_subkategori', 'kategori.nama_kategori'])
            ->paginate(5);

        return view('main.service_subkategori', compact('left_join_services'));
        //$data_detail_task = DB::table('detail_task')->get();
        //return view('main.detailtask', ['data_detail_task' => $data_detail_task]);
    }

    public function formServiceSubkategori()
    {
        $pilihan_serSub = services::all();
        $pilihan_kate = kategori::all();
        $pilihan_subKat = subkategori::all();
        return view('input_form.tambah_data_service_subkategori', compact('pilihan_serSub', 'pilihan_kate', 'pilihan_subKat'));
    }

    public function postservicesubkategori(Request $request)
    {
        $this->validate($request, [
            'id_services' => 'required',
            'id_subkategori' => 'required',
            'id_kategori' => 'required'
        ]);
        $data = $request->all();
        //dd($data);
        $post = [
            'id_services' => $data['id_services'],
            'id_subkategori' => $data['id_subkategori'],
            'id_kategori' => $data['id_kategori'],
        ];
        service_subkategori::create($post);
        return redirect('/servicesubkategori'); //->back();
    }

    public function editservicesubkategori(Request $request)
    {
        $data_service_subkategori = service_subkategori::find($request->route('id'));
        $pilihan_kate = kategori::all();
        $pilihan_subKat = subkategori::all();
        $pilihan_paket = services::all();
        //dd($pilihan_paket);
        //\\dd($request->route('id')
        //return view('edit.edit_detailtask', ['data_detail_task' => $data_detail_task]);
        //dd($data_service_subkategori);
        return view(
            'edit.edit_servicesubkategori',
            compact('data_service_subkategori', 'pilihan_kate', 'pilihan_subKat', 'pilihan_paket')
        );
        //     ['data_service_subkategori' => $data_service_subkategori, 'pilihan_kate' => $pilihan_kate],
        //     ['data_service_subkategori' => $data_service_subkategori, 'pilihan_subKat' => $pilihan_subKat],
        //     ['data_service_subkategori' => $data_service_subkategori, 'pilihan_paket' => $pilihan_paket]
        // );
    }

    public function updateservicesubkategori(Request $request)
    {
        $updateservicesubkategori = service_subkategori::find($request->route('id'));
        //Cara 1
        //$updatedetailtask->id_detail = $request->id_detail;
        //$updatedetailtask->id_task = $request->id_task;
        //$updatedetailtask->status = $request->status;
        //dd($request);
        //$updatedetailtask->save();
        //Cara2
        $updateservicesubkategori->update($request->all());
        return redirect('/servicesubkategori');
    }

    public function deleteservicesubkategori(Request $request)
    {
        $deleteservicesubkategori = service_subkategori::find($request->route('id'));
        $deleteservicesubkategori->delete();

        return redirect('/servicesubkategori');
    }
}
