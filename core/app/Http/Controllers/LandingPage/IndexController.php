<?php

namespace App\Http\Controllers\LandingPage;

use App\Http\Controllers\Controller;
use App\Models\contact;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    public function home()
    {
        return view('landing-page.home');
    }

    public function our_work()
    {
        return view('landing-page.our-works');
    }

    public function contact()
    {
        return view('landing-page.contact');
    }

    public function services()
    {
        return view('landing-page.services');
    }
    public function faq()
    {
        return view('landing-page.faq');
    }

}
