<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\kategori;
use App\Models\metode_pembayaran;
use App\Models\order;
use App\Models\contact;
use App\Models\subkategori;
use App\Models\services;
use App\Models\service_subkategori;
use App\Models\User;
use App\Models\pekerjaan;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Redirect;
use Auth;
use DateTime;
use PDF;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{
     /**
      * Create a new controller instance.
      *
      * @return void
      */
     // public function __construct()
     // {
     //      $this->middleware('auth');
     // }

     public function index(Request $request)
     {
          $kategori = kategori::get();
          foreach ($kategori as $item) {
               $item->subkategori = subkategori::where('id_kategori', $item->id)->get();
          }

          return view('main.katalog', compact('kategori'));
     }

     public function getDetail(Request $request)
     {
          $detail = service_subkategori::where('id_subkategori', $request->route('id'))
               ->leftJoin('subkategori', 'service_subkategori.id_subkategori', '=', 'subkategori.id')
               ->leftJoin('services', 'service_subkategori.id_services', '=', 'services.id')
               ->leftJoin('kategori', 'subkategori.id_kategori', '=', 'kategori.id')
               ->orderBy('subkategori.id', 'ASC')
               ->get();

          $subkategori = Subkategori::where('subkategori.id', $request->route('id'))
               ->leftJoin('kategori', 'subkategori.id_kategori', '=', 'kategori.id')
               ->first();

          return view('main.detailklien', compact('detail', 'subkategori'));
     }

     //public function removecheckout(Request $request)
     //{
     // $removecheckout = order::find($request->route('id'));
     //$removecheckout->delete();

     //return redirect('/katalog');
     //}

     public function detailkatalog(Request $request)
     {
          $cek = subkategori::where('id_kategori', $request->route('id'))
               ->where('nama_subkategori', 'not like', '%Custom%')
               ->get();
          // $service_subkategori = ServiceSubkategori::select(
          //      DB::raw('distinct service_subkategori.id_subkategori'),
          //      'subkategori.nama_subkategori',  
          //      'service_subkategori.id'
          //      )
          //      ->leftjoin('subkategori', 'service_subkategori.id_subkategori', '=', 'subkategori.id')
          //      ->where('service_subkategori.id_kategori', '=', $request->route('id'))
          //      ->groupBy('service_subkategori.id_subkategori')
          //      ->get();

          // $service_subkategori = ServiceSubkategori::whereNotNull('service_subkategori.id')
          // ->distinct()
          // ->leftjoin('subkategori', 'service_subkategori.id_subkategori', '=', 'subkategori.id')
          // ->where('service_subkategori.id_kategori', '=', $request->route('id'))
          // ->get();
          // dd($service_subkategori);

          if (count($cek) == 0) {
               // $services = services::leftJoin('service_subkategori', 'services.id', '=', 'service_subkategori.id_services')
               // ->where('service_subkategori.id_kategori', '=', $request->route('id'))
               // ->get();

               $services = services::where('service_subkategori.id_kategori', '=', $request->route('id'))
                    ->leftJoin('service_subkategori', 'services.id', '=', 'service_subkategori.id_services')
                    ->where('id_subkategori', NULL)
                    ->leftjoin('kategori', 'service_subkategori.id_kategori', '=', 'kategori.id')  
                    ->select('services.*', 'kategori.nama_kategori as id_kat')
                    ->get();

               $kategori = kategori::select('nama_kategori')
                    ->get();

               return view('main.katalog-paket', compact('services', 'kategori'));
          } else {
               $service_subkategori = subkategori::where('id_kategori', $request->route('id'))
                    ->where('nama_subkategori', 'not like', '%Custom%')
                    ->leftjoin('kategori', 'subkategori.id_kategori', '=', 'kategori.id')
                    ->select('subkategori.*', 'nama_kategori')
                    ->get();
               return view('main.katalog-sub', compact('service_subkategori'));
          }
     }

     public function detailpaket(Request $request)
     {
          // $services = services::leftJoin('service_subkategori', 'services.id', '=', 'service_subkategori.id_services')
          //      ->where('service_subkategori.id_subkategori', '=', $request->route('id'))
          //      ->get();

          $services = services::select('services.*', 'subkategori.nama_subkategori as id_kat')
               ->where('service_subkategori.id_subkategori', '=', $request->route('id'))
               ->leftJoin('service_subkategori', 'services.id', '=', 'service_subkategori.id_services')
               ->leftjoin('subkategori', 'service_subkategori.id_subkategori', '=', 'subkategori.id')
               ->where('subkategori.nama_subkategori', 'not like', '%Custom%')
               ->get();

          return view('main.katalog-paket', compact('services'));
     }

     public function checkout(Request $request)
     {
          $services = services::select('services.*', 'service_subkategori.id as id_service_sub')
               ->leftjoin('service_subkategori', 'services.id', '=', 'service_subkategori.id_services')
               ->where('services.id', '=', $request->route('id'))
               ->first();

          $metode = metode_pembayaran::all();

          return view('main.checkout', compact('services', 'metode'));
     }

     public function bayar(Request $request)
     {
          $data = $request->all();
          $post = [
               'id_service_subkategori' => $data['id_service_sub'],
               'total_harga' => $data['harga'],
               'id_users' => Auth::user()->id,
               'date_time' => date('Y-m-d'),
               'id_metode_pembayaran' => $data['metode_bayar'],
               'tipe_order' => $data['tipe_order'],
               'sisa_bayar' => $data['sisa_bayar'],
               'total_dibayar' => $data['total_dibayar'],
               'status_pembayaran' => $data['status_pembayaran']
          ];

          $order = order::create($post);
          if ($order) {
               $create = [
                    'id_order' => $order->id,
                    'pre-produksi' => 0,
                    'produksi' => 0,
                    'post_produksi' => 0,
                    'selesai' => 0,
                    'link_project' => NULL,
                    'size_project' => NULL,
                    'diunggah' => NULL
               ];

               pekerjaan::create($create);
          }
          $paket = services::where('id', '=', $data['id_service'])
               ->first();

          return Redirect::to('https://wa.me/6281319303492?text=Halo,%20Saya%20ingin%20membeli%20paket:%20' . $paket->jenis_paket . '%0AHarga:' . $paket->harga . '%20Terimakasih!');
     }

     public function bayarCustom(Request $request)
     {
          $data = $request->all();
          $post = [
               'total_harga' => $data['total_harga'],
               'id_users' => Auth::user()->id,
               'date_time' => date('Y-m-d'),
               'tipe_order' => $data['tipe_order'],
               'sisa_bayar' => $data['sisa_bayar'],
               'total_dibayar' => $data['total_dibayar'],
               'status_pembayaran' => $data['status_pembayaran']
          ];

          $order = order::create($post);
          $id = $order['id'];

          if ($order) {
               $create = [
                    'id_order' => $id,
                    'pre-produksi' => 0,
                    'produksi' => 0,
                    'post_produksi' => 0,
                    'selesai' => 0,
                    'link_project' => NULL,
                    'size_project' => NULL,
                    'diunggah' => NULL
               ];

               pekerjaan::create($create);
          }
          $detail_paket = $data['paket_custom'];
          $id_kat_apa = $data['id'];
   
          return Redirect::to('https://wa.me/6281319303492?text=Halo,%20Saya%20ingin%20membeli%20paket%20custom%20untuk%20kategori/subkategori%20'.$id_kat_apa.'.%0ABerikut%20detail%20spesifikasi%20paket:%0A%0A'.$detail_paket.'.%0A%0AHarganya%20berapa%20ya?%20Terimakasih!');
     }

     public function transaksi()
     {
          $transactions = order::leftjoin('service_subkategori', 'order.id_service_subkategori', '=', 'service_subkategori.id')
               ->leftjoin('services', 'service_subkategori.id_services', '=', 'services.id')
               ->select('order.*', 'services.jenis_paket', 'services.harga')
               ->where('id_users', Auth::user()->id)
               ->get();
          return view('main.transaksi', compact('transactions'));
     }

     public function postBuktiBayar(Request $request)
     {
          if ($request->hasfile('bukti_bayar_lunas')) {

               $request->validate([
                    'bukti_bayar_lunas' => 'required|image|mimes:jpeg,png,jpg|max:1024',
               ]);

               $file = $request->file('bukti_bayar_lunas');

               $random = rand();
               $nama_file = $random . $file->getClientOriginalName();
               $tujuan_upload = 'public/app-assets/images/pages/buktiBayar';
               $full_path = $tujuan_upload . '/' . $nama_file;
               $file->move($tujuan_upload, $nama_file);

               $order_update = order::find($request->input('id'));
               $order_update->bukti_bayar_lunas = $full_path;
               $order_update->update();

               if ($order_update) {
                    //redirect dengan pesan sukses
                    return redirect()->route('transaksi');
               }
          }
     }

     public function postBuktiBayarDP(Request $request)
     {
          if ($request->hasfile('bukti_bayar_dp')) {

               // $validate = $request->validate([
               //      'bukti_bayar_dp' => 'required|image|mimes:jpeg,png,jpg|max:1024',
               // ],
               // [
               //      'bukti_bayar_dp.required' => 'Tidak ada file yang diunggah!',
               //      'bukti_bayar_dp.image' => 'File harus berupa gambar!',
               //      'bukti_bayar_dp.mimes' => 'Tipe file harus jpeg, png, atau png!',
               //      'bukti_bayar_dp.max' => 'Ukuran file maksimal 1 MB!'
               // ]);

               $validate = Validator::make(
                    $request->all(),
                    [
                         'bukti_bayar_dp' => 'required|image|mimes:jpeg,png,jpg|max:1024'
                    ]
               );

               if ($validate->fails()) {
                    return redirect()->back()->withErrors($validate);
               }

               $file = $request->file('bukti_bayar_dp');

               $random = rand();
               $nama_file = $random . $file->getClientOriginalName();
               $tujuan_upload = 'public/app-assets/images/pages/buktiBayarDP';
               $full_path = $tujuan_upload . '/' . $nama_file;
               $file->move($tujuan_upload, $nama_file);

               $order_update = order::find($request->input('id'));
               $order_update->bukti_bayar_dp = $full_path;
               $order_update->update();
               if ($order_update) {
                    //redirect dengan pesan sukses
                    return redirect()->route('transaksi');
               }
          }
     }

     public function invoice(Request $request)
     {
          $order = order::leftjoin('service_subkategori', 'order.id_service_subkategori', '=', 'service_subkategori.id')
               ->leftjoin('services', 'service_subkategori.id_services', '=', 'services.id')
               ->select('order.*', 'services.jenis_paket', 'services.harga')
               ->where('order.id', '=', $request->route('id'))
               ->first();

          $user = User::select('name', 'email', 'no_hp_user', 'alamat_user')
               ->where('id', Auth::user()->id)
               ->first();

          $contact = contact::first();

          return view('main.invoice', compact('contact', 'order', 'user'));
     }

     public function downloadPDF(Request $request)
     {
          $order = order::leftjoin('service_subkategori', 'order.id_service_subkategori', '=', 'service_subkategori.id')
               ->leftjoin('services', 'service_subkategori.id_services', '=', 'services.id')
               ->select('order.*', 'services.jenis_paket', 'services.harga')
               ->where('order.id', '=', $request->route('id'))
               ->first();

          $user = User::select('name', 'email', 'no_hp_user', 'alamat_user')
               ->where('id', Auth::user()->id)
               ->first();

          $contact = contact::first();

          $pdf = PDF::loadView('main.contoh', compact('order', 'user', 'contact'))->setOptions(['defaultFont' => 'sans-serif']);;
          return $pdf->download('invoice.pdf');
     }

     public function projekSaya(){
          $semua = pekerjaan::select('pekerjaan.*','services.jenis_paket', 'order.*', 'services.foto')
                    ->where('pre_produksi', 1)
                    ->leftjoin('order', 'pekerjaan.id_order', '=', 'order.id')
                    ->where('id_users', '=', Auth::user()->id)
                    ->leftjoin('service_subkategori', 'order.id_service_subkategori', '=', 'service_subkategori.id')
                    ->leftjoin('services', 'service_subkategori.id_services', '=', 'services.id')
                    ->get();

          $progress = pekerjaan::select('pekerjaan.*', 'services.jenis_paket', 'order.*', 'services.foto')
               ->where('pre_produksi', 1)
               ->where('selesai', 0)
               ->leftjoin('order', 'pekerjaan.id_order', '=', 'order.id')
               ->where('id_users', '=', Auth::user()->id)
               ->leftjoin('service_subkategori', 'order.id_service_subkategori', '=', 'service_subkategori.id')
               ->leftjoin('services', 'service_subkategori.id_services', '=', 'services.id')
               ->get();

          $selesai = pekerjaan::select('pekerjaan.*', 'services.jenis_paket', 'order.*', 'services.foto')
               ->where('selesai', 1)
               ->where('selesai', 1)
               ->leftjoin('order', 'pekerjaan.id_order', '=', 'order.id')
               ->where('id_users', '=', Auth::user()->id)
               ->leftjoin('service_subkategori', 'order.id_service_subkategori', '=', 'service_subkategori.id')
               ->leftjoin('services', 'service_subkategori.id_services', '=', 'services.id')
               ->get();

          return view('main.projekSaya', compact('semua', 'progress', 'selesai'));
     }

     public function feedback()
     {
          return view('main.feedback');
     }

     public function postfeedback(Request $request)
     {
          $data = $request->all();

          return Redirect::to('https://wa.me/6281319303492?text=' . $data['feedback']);
     }

     public function trackingProgres()
     {
          $pekerjaan = pekerjaan::select('pekerjaan.*', 'services.jenis_paket')
               ->where('pre_produksi', '1')
               ->leftjoin('order', 'pekerjaan.id_order', '=', 'order.id')
               ->where('id_users', '=', Auth::user()->id)
               ->leftjoin('service_subkategori', 'order.id_service_subkategori', '=', 'service_subkategori.id')
               ->leftjoin('services', 'service_subkategori.id_services', '=', 'services.id')
               ->orderBy('id_order', 'desc')
               ->get();

          return view('main.tracking-progres', compact('pekerjaan'));
     }
}
