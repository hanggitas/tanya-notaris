<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\user;
use App\Models\Users;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function user()
    {
        $userss = user::paginate(5);
        return view('main.user', compact('userss'));
    }

    public function postuser(Request $request)
    {
        $data = $request->all();
        $post = [
            'name' => $data['name'],
            'email' => $data['email'],
            'role' => $data['role'],
            'password' => $data['password'],
            'foto' => $data['foto'],
            'no_hp_user' => $data['no_hp_user'],
            'alamat_user' => $data['alamat_user']
        ];
        user::create($post);
        return redirect()->route('user');
    }

    public function edit($id)
    {
        $user = user::find($id);
        return view('input_form.ubah_data_user', compact('user'));
    }

    public function update(Request $request, $id)
    {
        // $this->validate($request, [
        //     'name_user' => 'name_user',
        //     'email_user' => 'email_user',
        //     'password' => 'password',
        //     'foto' => 'foto',
        //     'no_hp_user' => 'no_hp_user',
        //     'alamat_user' => 'alamat_user'
        // ]);

        $users = user::find($id);
        $users->name_user = $request->input('name_user');
        $users->email_user = $request->input('email_user');
        $users->password = $request->input('password');
        $users->foto = $request->input('foto');
        $users->no_hp_user = $request->input('no_hp_user');
        $users->alamat_user = $request->input('alamat_user');
        $users->update();

        if ($users) {
            //redirect dengan pesan sukses
            return redirect()->route('user')->with(['success' => 'Data Berhasil Diupdate!']);
        } else {
            //redirect dengan pesan error
            return redirect()->route('user')->with(['error' => 'Data Gagal Diupdate!']);
        }
    }

    public function delete($id)
    {
        $users = User::find($id);
        $users->delete();
        return redirect()->route('users')->with('status', 'Data Berhasil Dihapus!');
    }
}
