<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FaqController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\SubkategoriController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/index', [App\Http\Controllers\LandingPage\IndexController::class, 'index'])->name('index');
Route::get('/contact', [App\Http\Controllers\LandingPage\IndexController::class, 'contact'])->name('contact');
Route::get('/faq-landing', [App\Http\Controllers\LandingPage\IndexController::class, 'faq'])->name('faq-landing');
Route::get('/beranda', [App\Http\Controllers\LandingPage\IndexController::class, 'home'])->name('beranda');
Route::get('/our-works', [App\Http\Controllers\LandingPage\IndexController::class, 'our_work'])->name('our-works');
Route::get('/services', [App\Http\Controllers\LandingPage\IndexController::class, 'services'])->name('services');

Auth::routes();

Route::get('/progres-proyek', [App\Http\Controllers\ClientController::class, 'trackingProgres'])->name('trackingProgres');
Route::get('/feedback', [App\Http\Controllers\ClientController::class, 'feedback'])->name('feedback');
Route::post('/post-feedback', [App\Http\Controllers\ClientController::class, 'postfeedback'])->name('post-feedback');
Route::get('/proyek-saya', [App\Http\Controllers\ClientController::class, 'projekSaya'])->name('projek');
Route::get('/invoice/{id}', [App\Http\Controllers\ClientController::class, 'invoice']);
Route::get('/download-pdf/{id}', [App\Http\Controllers\ClientController::class, 'downloadPDF']);
Route::get('/transaksi', [App\Http\Controllers\ClientController::class, 'transaksi'])->name('transaksi');
Route::post('/upload-bukti-pembayaran', [App\Http\Controllers\ClientController::class, 'postBuktiBayar'])->name('upload-bukti-bayar');
Route::post('/upload-bukti-pembayaran-dp', [App\Http\Controllers\ClientController::class, 'postBuktiBayarDP'])->name('upload-bukti-bayar-dp');
Route::get('/katalog', [App\Http\Controllers\DashboardController::class, 'katalog'])->name('katalog');

Route::get('/detail-paket', [App\Http\Controllers\ClientController::class, 'detailpaket'])->name('detailpaket');
Route::get('/katalog', [App\Http\Controllers\ClientController::class, 'index']);
Route::get('/katalog/detail-katalog/{id}', [App\Http\Controllers\ClientController::class, 'detailkatalog']);
Route::get('/katalog/detail-katalog/detail-paket/{id}', [App\Http\Controllers\ClientController::class, 'detailpaket']);
Route::get('/katalog/detail-katalog/detail-paket/checkout/{id}', [App\Http\Controllers\ClientController::class, 'checkout']);
Route::post('/bayar', [App\Http\Controllers\ClientController::class, 'bayar'])->name('bayar');
Route::post('/bayar-custom', [App\Http\Controllers\ClientController::class, 'bayarCustom'])->name('bayarCustom');
//Route::get('/checkout', [App\Http\Controllers\ClientController::class, 'checkout'])->name('checkout');


Route::get('/katalog', [App\Http\Controllers\ClientController::class, 'index']);
Route::get('/detail-item/{id}', [App\Http\Controllers\ClientController::class, 'getDetail']);

Route::get('/', [App\Http\Controllers\DashboardController::class, 'dashboard'])->name('dashboard');
Route::post('/post-targer', [App\Http\Controllers\DashboardController::class, 'posttarget'])->name('target');
Route::get('/akun', [App\Http\Controllers\AkunController::class, 'akun'])->name('akun');
Route::get('/edit', [UpdateProfileInformationController::class, 'edit'])->name('edit.akun');
// Route::put('/update', [UpdateProfileInformationController::class, 'update'])->name('update.akun');
// Route::post('/updateakun', [App\Http\Controllers\AkunController::class, 'update'])->name('updateakun');
Route::get('/password', [App\Http\Controllers\DashboardController::class, 'password'])->name('password');

Route::get('/portofolio', [App\Http\Controllers\PortofolioController::class, 'portofolio'])->name('portofolio');
Route::get('/tambah-data-portofolio', [App\Http\Controllers\PortofolioController::class, 'tambah_data_portofolio'])->name('tambah_data_portofolio');
Route::get('/edit-portofolio/{id}', [App\Http\Controllers\PortofolioController::class, 'edit'])->name('edit');
Route::put('/update-portofolio/{id}', [App\Http\Controllers\PortofolioController::class, 'update'])->name('update');
Route::get('/delete-portofolio/{id}', [App\Http\Controllers\PortofolioController::class, 'delete'])->name('delete');

Route::post('/postservis', [App\Http\Controllers\PortofolioController::class, 'postportofolio'])->name('data_portofolio');

Route::get('/servis', [App\Http\Controllers\ServicesController::class, 'servis'])->name('servis');
Route::get('/tambah-data-servis', [App\Http\Controllers\ServicesController::class, 'tambah_data_service'])->name('tambah_data_service');
Route::post('/postservis', [App\Http\Controllers\ServicesController::class, 'postservice'])->name('data_service');
Route::get('/servis/{id}/edit', [App\Http\Controllers\TaskController::class, 'edit'])->name('edit');
Route::post('/servis/{id}/update', [App\Http\Controllers\TaskController::class, 'update'])->name('update');
Route::get('/servis/{id}/delete', [App\Http\Controllers\TaskController::class, 'delete'])->name('delete');

Route::get('/faq', [App\Http\Controllers\FaqController::class, 'faq'])->name('faq');
Route::get('/add-faq', [App\Http\Controllers\FaqController::class, 'addForm'])->name('add-faq');
Route::post('/post-faq', [App\Http\Controllers\FaqController::class, 'postData'])->name('post-faq');

Route::post('/postportofolio', [App\Http\Controllers\PortofolioController::class, 'postportofolio'])->name('data_portofolio');

Route::get('/servis', [App\Http\Controllers\ServicesController::class, 'servis'])->name('servis');
Route::get('/tambah-data-service', [App\Http\Controllers\ServicesController::class, 'tambah_data_service'])->name('tambah_data_service');
Route::post('/postservice', [App\Http\Controllers\ServicesController::class, 'postservice'])->name('data_service');
Route::get('/edit-service/{id}', [App\Http\Controllers\ServicesController::class, 'edit']);
Route::put('/update-service/{id}', [App\Http\Controllers\ServicesController::class, 'update']);
Route::get('/delete-service/{id}', [App\Http\Controllers\ServicesController::class, 'delete']);

Route::get('/faq', [App\Http\Controllers\FaqController::class, 'faq'])->name('faq');
Route::get('/tambah-data-faq', [App\Http\Controllers\FaqController::class, 'tambah_data_faq'])->name('tambah_data_faq');
Route::post('/postfaq', [App\Http\Controllers\FaqController::class, 'postfaq'])->name('data_faq');
Route::get('/edit-faq/{id}', [App\Http\Controllers\FaqController::class, 'edit']);
Route::put('/update-faq/{id}', [App\Http\Controllers\FaqController::class, 'update']);
Route::get('/delete-faq/{id}', [App\Http\Controllers\FaqController::class, 'delete']);

Route::get('/kategori', [App\Http\Controllers\KategoriController::class, 'kategori'])->name('kategori');
Route::get('/tambah-data-kategori', [App\Http\Controllers\KategoriController::class, 'tambah_data_kategori'])->name('tambah_data_kategori');
Route::post('/postkategori', [App\Http\Controllers\KategoriController::class, 'postkategori'])->name('data_kategori');

Route::get('/kategori/{id}/edit', [App\Http\Controllers\KategoriController::class, 'edit'])->name('edit');
Route::post('/kategori/{id}/update', [App\Http\Controllers\KategoriController::class, 'update'])->name('update');
Route::get('/kategori/{id}/delete', [App\Http\Controllers\KategoriController::class, 'delete'])->name('delete');
Route::get('/form-kategori', [App\Http\Controllers\KategoriController::class, 'form-kategori'])->name('form-kategori');

Route::get('/edit-kategori/{id}', [App\Http\Controllers\KategoriController::class, 'edit']);
Route::put('/update-kategori/{id}', [App\Http\Controllers\KategoriController::class, 'update']);
Route::get('/delete-kategori/{id}', [App\Http\Controllers\KategoriController::class, 'delete']);

Route::get('/subkategori', [App\Http\Controllers\SubkategoriController::class, 'subkategori'])->name('subkategori');

Route::get('/tambah-data-subkategori', [App\Http\Controllers\SubkategoriController::class, 'tambah_data_subkategori'])->name('tambah_data_subkategori');


Route::post('/postsubkategori', [App\Http\Controllers\SubkategoriController::class, 'postsubkategori'])->name('data_subkategori');
Route::get('/edit-subkategori/{id}', [App\Http\Controllers\SubkategoriController::class, 'edit']);
Route::put('/update-subkategori/{id}', [App\Http\Controllers\SubkategoriController::class, 'update']);
Route::get('/delete-subkategori/{id}', [App\Http\Controllers\SubkategoriController::class, 'delete']);

Route::get('/order', [App\Http\Controllers\OrderController::class, 'order'])->name('order');
Route::get('/formorder', [App\Http\Controllers\OrderController::class, 'formorder'])->name('formorder');
Route::post('/postorder', [App\Http\Controllers\OrderController::class, 'postorder'])->name('data_order');
Route::get('/edit-order/{id}', [App\Http\Controllers\OrderController::class, 'edit']);
Route::post('/order/{id}', [App\Http\Controllers\OrderController::class, 'updateorder'])->name('updateorder');
Route::get('/delete-order/{id}', [App\Http\Controllers\OrderController::class, 'delete']);
Route::get('/custom-order/{id}', [App\Http\Controllers\OrderController::class, 'custom']);
Route::post('/post-custom', [App\Http\Controllers\OrderController::class, 'postcustom'])->name('custom_order');

Route::get('/detail', [App\Http\Controllers\DetailController::class, 'detail'])->name('detail');
Route::get('/formdetail', [App\Http\Controllers\DetailController::class, 'formdetail'])->name('formdetail');
Route::post('/postdetail', [App\Http\Controllers\DetailController::class, 'postdetail'])->name('data_detail');
Route::get('/delete-detail/{id}', [App\Http\Controllers\DetailController::class, 'delete']);

Route::get('/kontak', [App\Http\Controllers\ContactController::class, 'kontak'])->name('kontak');
Route::get('/formkontak', [App\Http\Controllers\ContactController::class, 'formkontak'])->name('formkontak');
Route::post('/postkontak', [App\Http\Controllers\ContactController::class, 'postkontak'])->name('data_kontak');
Route::get('/kontak/{id}/edit', [App\Http\Controllers\ContactController::class, 'editkontak'])->name('edit_kontak');
Route::post('/kontak/{id}/update', [App\Http\Controllers\ContactController::class, 'updatekontak'])->name('update_kontak');
Route::get('/kontak/{id}/delete', [App\Http\Controllers\ContactController::class, 'deletekontak'])->name('delete_kontak');

Route::get('/progres', [App\Http\Controllers\ProgresController::class, 'progres'])->name('progres');
Route::post('/postprogres', [App\Http\Controllers\ProgresController::class, 'postprogres'])->name('data_progres');
Route::get('/edit-progres/{id}', [App\Http\Controllers\ProgresController::class, 'edit']);
Route::put('/update-progres/{id}', [App\Http\Controllers\ProgresController::class, 'update']);
Route::get('/delete-progres/{id}', [App\Http\Controllers\ProgresController::class, 'delete']);

Route::get('/task', [App\Http\Controllers\TaskController::class, 'task'])->name('task');
Route::get('/task/{id}/edit', [App\Http\Controllers\TaskController::class, 'edittask'])->name('edittask');
Route::post('/task/{id}/update', [App\Http\Controllers\TaskController::class, 'updatetask'])->name('updatetask');
Route::get('/task/{id}/delete', [App\Http\Controllers\TaskController::class, 'deletetask'])->name('deletetask');
Route::get('/formtask', [App\Http\Controllers\TaskController::class, 'formtask'])->name('formtask');
Route::post('/posttask', [App\Http\Controllers\TaskController::class, 'posttask'])->name('data_task');

Route::get('/detailtask', [App\Http\Controllers\DetailTaskController::class, 'detailtask'])->name('detailtask');
Route::get('/detailtask/{id}/edit', [App\Http\Controllers\DetailTaskController::class, 'editdetailtask'])->name('editdetailtask');
Route::post('/detailtask/{id}/update', [App\Http\Controllers\DetailTaskController::class, 'updatedetailtask'])->name('updatedetailtask');
Route::get('/detailtask/{id}/delete', [App\Http\Controllers\DetailTaskController::class, 'deletedetailtask'])->name('deletedetailtask');
Route::get('/formdetailtask', [App\Http\Controllers\DetailTaskController::class, 'formdetailtask'])->name('formdetailtask');
Route::post('/postdetailtask', [App\Http\Controllers\DetailTaskController::class, 'postdetailtask'])->name('data_detail_task');

Route::get('/metodepembayaran', [App\Http\Controllers\MetodePembayaranController::class, 'metodepembayaran'])->name('metodepembayaran');
Route::get('/metodepembayaran/{id}/edit', [App\Http\Controllers\MetodePembayaranController::class, 'editmetodepembayaran'])->name('editmetodepembayaran');
Route::post('/metodepembayaran/{id}/update', [App\Http\Controllers\MetodePembayaranController::class, 'updatemetodepembayaran'])->name('updatemetodepembayaran');
Route::get('/metodepembayaran/{id}/delete', [App\Http\Controllers\MetodePembayaranController::class, 'deletemetodepembayaran'])->name('deletemetodepembayaran');
Route::get('/formmetodepembayaran', [App\Http\Controllers\MetodePembayaranController::class, 'formmetodepembayaran'])->name('formmetodepembayaran');
Route::post('/postmetodepembayaran', [App\Http\Controllers\MetodePembayaranController::class, 'postmetodepembayaran'])->name('data_metode_pembayaran');

Route::get('/preview', [App\Http\Controllers\PreviewController::class, 'preview'])->name('preview');
Route::get('/preview/{id}/edit', [App\Http\Controllers\PreviewController::class, 'editpreview'])->name('editpreview');
Route::post('/preview/{id}/update', [App\Http\Controllers\PreviewController::class, 'updatepreview'])->name('updatepreview');
Route::get('/preview/{id}/delete', [App\Http\Controllers\PreviewController::class, 'deletepreview'])->name('deletepreview');
Route::get('/formpreview', [App\Http\Controllers\PreviewController::class, 'formpreview'])->name('formpreview');
Route::post('/postpreview', [App\Http\Controllers\PreviewController::class, 'postpreview'])->name('data_preview');

Route::get('/servicesubkategori', [App\Http\Controllers\ServiceSubkategoriController::class, 'service_subkategori'])->name('servicesubkategori');
Route::get('/form-Service-Subkategori', [App\Http\Controllers\ServiceSubkategoriController::class, 'formServiceSubkategori'])->name('form_Service_Subkategori');
Route::post('/postservicesubbkategori', [App\Http\Controllers\ServiceSubkategoriController::class, 'postservicesubkategori'])->name('data_service_subkategori');
Route::get('/servicesubkategori/{id}/edit', [App\Http\Controllers\ServiceSubkategoriController::class, 'editservicesubkategori'])->name('edit_servicesubkategori');
Route::post('/servicesubkategori/{id}/update', [App\Http\Controllers\ServiceSubkategoriController::class, 'updateservicesubkategori'])->name('update_servicesubkategori');
Route::get('/servicesubkategori/{id}/delete', [App\Http\Controllers\ServiceSubkategoriController::class, 'deleteservicesubkategori'])->name('deletesubkategori');

Route::get('/pekerjaan', [App\Http\Controllers\PekerjaanController::class, 'pekerjaan'])->name('pekerjaan');
Route::get('/form-pekerjaan', [App\Http\Controllers\PekerjaanController::class, 'formpekerjaan'])->name('form_pekerjaan');
Route::post('/postpekerjaan', [App\Http\Controllers\PekerjaanController::class, 'postpekerjaan'])->name('data_pekerjaan');
Route::get('/pekerjaan/{id}/edit', [App\Http\Controllers\PekerjaanController::class, 'edittpekerjaan'])->name('edit_pekerjaan');
Route::post('/pekerjaan/{id}/update', [App\Http\Controllers\PekerjaanController::class, 'updatepekerjaan'])->name('update_pekerjaan');
Route::get('/pekerjaan/{id}/delete', [App\Http\Controllers\PekerjaanController::class, 'hapuspekerjaan'])->name('deletepekerjaan');

Route::get('/target', [App\Http\Controllers\PekerjaanController::class, 'target'])->name('target');
Route::get('/form-target', [App\Http\Controllers\PekerjaanController::class, 'formtarget'])->name('form_target');
Route::post('/posttarget', [App\Http\Controllers\PekerjaanController::class, 'posttarget'])->name('data_target');
Route::get('/target/{id}/edit', [App\Http\Controllers\PekerjaanController::class, 'edittarget'])->name('edit_target');
Route::post('/target/{id}/update', [App\Http\Controllers\PekerjaanController::class, 'updatetarget'])->name('update_target');
Route::get('/target/{id}/delete', [App\Http\Controllers\PekerjaanController::class, 'hapustarget'])->name('deletetarget');

Route::get('/tambah-data-faq', [App\Http\Controllers\FaqController::class, 'tambah_data_faq'])->name('tambah_data_faq');
Route::post('/post-faq', [App\Http\Controllers\FaqController::class, 'post_faq'])->name('data_faq');


Route::get('/user', [App\Http\Controllers\UserController::class, 'user'])->name('user');
Route::post('/postuser', [App\Http\Controllers\UserController::class, 'postuser'])->name('data_user');
Route::get('/edit-user/{id}', [App\Http\Controllers\UserController::class, 'edit']);
Route::put('/update-user/{id}', [App\Http\Controllers\UserController::class, 'update']);
Route::get('/delete-user/{id}', [App\Http\Controllers\UserController::class, 'delete']);

Route::get('/checkout', [App\Http\Controllers\ClientController::class, 'checkout'])->name('checkout');
Route::post('/bayar', [App\Http\Controllers\ClientController::class, 'bayar'])->name('bayar');


Route::get('/detailklien', [App\Http\Controllers\DashboardController::class, 'detailklien'])->name('detailklien');

// Route::get('/subkategori', [App\Http\Controllers\DashboardController::class, 'subkategori'])->name('subkategori');
//Route::get('/formsubkategori', [App\Http\Controllers\SubkategoriController::class, 'formsubkategori'])->name('formsubkategori');
//Route::post('/postsubkategori', [App\Http\Controllers\SubkategoriController::class, 'postsubkategori'])->name('data_subkategori');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
