@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content">
    <div class="row">
        <div class="col-lg-12 text-center">
        @php
            $user = Auth::user()->name;
        @endphp
        <h1 class="mt-3">Selamat Datang, {{$user}}! 👋</h1>
            <p class="mt-1 mb-1 pb-75">
                Selamat datang .<br/>
                Please leave us a feedback to let us know what to improve. Thank you!
            </p>
        </div>
    </div>
</div>
@endsection