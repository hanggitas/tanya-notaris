
@section('menu')
<div class="main-menu menu-fixed menu-dark menu-accordion" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item me-auto"><a class="navbar-brand" href="">
                <span class="brand-logo">
                    <img src="{{ asset('app-assets/images/ico/Logo.png') }}"/>
                </span>
                    <h2 class="brand-text">Tanya Notaris</h2>
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc" data-ticon="disc"></i></a></li>
        </ul>
    </div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            @if(Auth::user()->role == 'admin')
                <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('dashboard') }}"><i data-feather="home"></i><span class="menu-title text-truncate" data-i18n="Dashboards">Dashboards</span></a>
                </li>
                
                <li class=" navigation-header"><span>Menu</span><i data-feather="more-horizontal"></i>
                </li>

                <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('user') }}"><i data-feather="users"></i><span class="menu-title text-truncate" data-i18n="Datatable">User Klien</span></a>
                </li>

                <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="file-text"></i><span class="menu-title text-truncate" data-i18n="Daftar Layanan">Daftar Layanan</span></a>
                    <ul class="menu-content">
                        <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('servis') }}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Datatable">Paket Layanan</span></a>
                        </li>
                        <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('subkategori') }}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Datatable">Subkategori</span></a>
                        </li>
                        <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('kategori') }}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Datatable">Kategori</span></a>
                        </li>
                        <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('servicesubkategori') }}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Datatable">Tabel Service Subkategori</span></a>
                        </li>
                    </ul>
                </li>

                <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="layout"></i><span class="menu-title text-truncate" data-i18n="Daftar Layanan">Daftar Konten</span></a>
                    <ul class="menu-content">
                        <li><a class="d-flex align-items-center" href="app-invoice-list.html"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">Artikel</span></a>
                        </li>
                        <li><a class="d-flex align-items-center" href="app-invoice-preview.html"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">Banner</span></a>
                        </li>
                        <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('faq') }}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Datatable">FAQ</span></a>
                        </li>
                        <li><a class="d-flex align-items-center" href="app-invoice-edit.html"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Edit">Klien</span></a>
                        </li>
                        <li><a class="d-flex align-items-center" href="app-invoice-add.html"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Add">Testimoni</span></a>
                        </li>
                    </ul>
                </li>

                <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="file-text"></i><span class="menu-title text-truncate" data-i18n="Daftar Layanan">Proyek Klien</span></a>
                    <ul class="menu-content">
                        <li><a class="d-flex align-items-center" href="app-invoice-list.html"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">List</span></a>
                        </li>
                        <li><a class="d-flex align-items-center" href="app-invoice-preview.html"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">Preview</span></a>
                        </li>
                        <li><a class="d-flex align-items-center" href="app-invoice-edit.html"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Edit">Edit</span></a>
                        </li>
                        <li><a class="d-flex align-items-center" href="app-invoice-add.html"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Add">Add</span></a>
                        </li>
                    </ul>
                </li>

                <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="file-text"></i><span class="menu-title text-truncate" data-i18n="Daftar Layanan">Order Klien</span></a>
                    <ul class="menu-content">
                        <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('metodepembayaran') }}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Datatable">Metode Pembayaran</span></a>
                        </li>
                        <li><a class="d-flex align-items-center" href="app-invoice-preview.html"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">Preview</span></a>
                        </li>
                        <li><a class="d-flex align-items-center" href="app-invoice-edit.html"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Edit">Edit</span></a>
                        </li>
                        <li><a class="d-flex align-items-center" href="app-invoice-add.html"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Add">Add</span></a>
                        </li>
                    </ul>
                </li>

                {{-- <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('detail') }}"><i data-feather="grid"></i><span class="menu-title text-truncate" data-i18n="Datatable">Tabel detail</span></a>
                </li> --}}
                {{-- <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('progres') }}"><i data-feather="grid"></i><span class="menu-title text-truncate" data-i18n="Datatable">Tabel Progres</span></a>
                </li>
                </li> --}}
               
                {{-- <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('preview') }}"><i data-feather="grid"></i><span class="menu-title text-truncate" data-i18n="Datatable">Tabel Preview</span></a>
                </li> --}}
                {{-- <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('checkout') }}"><i data-feather="grid"></i><span class="menu-title text-truncate" data-i18n="Datatable">Checkout</span></a>
                </li> --}}
                <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('pekerjaan') }}"><i data-feather="grid"></i><span class="menu-title text-truncate" data-i18n="Datatable">Tabel Pekerjaan</span></a>
                </li>
                <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('target') }}"><i data-feather="grid"></i><span class="menu-title text-truncate" data-i18n="Datatable">Tabel Target Penjualan</span></a>
                </li>
            @else
                <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('trackingProgres') }}"><i data-feather='trending-up'></i><span class="menu-title text-truncate" data-i18n="Dashboards">Progres Proyek</span></a>
                </li>
                <li class=" navigation-header"><span>Menu</span><i data-feather="more-horizontal"></i>
                </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('projek') }}"><i data-feather='folder'></i><span class="menu-title text-truncate" data-i18n="Datatable">Proyek Saya</span></a>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="{{ url('/katalog') }}"><i data-feather='shopping-bag'></i><span class="menu-title text-truncate" data-i18n="Datatable">Katalog</span></a>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('transaksi') }}"><i data-feather='file-text'></i></i><span class="menu-title text-truncate" data-i18n="Datatable">Transaksi Saya</span></a>
                    </li>       
                    <li class=" nav-item"><a class="d-flex align-items-center" href="{{ route('feedback') }}"><i data-feather='heart'></i><span class="menu-title text-truncate" data-i18n="Datatable">Feedback</span></a>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href=""><i data-feather='settings'></i><span class="menu-title text-truncate" data-i18n="Datatable">Pengaturan Profil</span></a>
                    </li>
                    <li class=" nav-item"><a class="d-flex align-items-center" href=""><i data-feather='log-out'></i><span class="menu-title text-truncate" data-i18n="Datatable">Keluar</span></a>
                    </li>
            @endif
        </ul>
    </div>
</div>
@endsection