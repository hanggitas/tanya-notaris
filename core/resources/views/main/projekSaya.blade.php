@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h1 class="mt-3">Proyek Saya</h1>
            <p class="mt-1 mb-1 pb-75">
                Tempat semua proyek anda disimpan. Anda bisa melihat dan mengunduh proyek yang telah selesai
            </p>
        </div>
        <ul class="nav nav-pills justify-content-center">
            <li class="nav-item">
                <a class="btn btn-primary round active mx-1" id="all-project" data-bs-toggle="pill" href="#all" aria-expanded="true">Semua</a>
            </li>
            <li class="nav-item">
                <a class="btn btn-primary round mx-1" id="on-going-project" data-bs-toggle="pill" href="#on-going" aria-expanded="false">Dalam Progres</a>
            </li>
            <li class="nav-item">
                <a class="btn btn-primary round mx-1" id="finished-project" data-bs-toggle="pill" href="#finished" aria-expanded="false">Selesai</a>
            </li>
        </ul>

        <!-- Semua -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="all" aria-labelledby="all-project" aria-expanded="true">
                <div class="row justify-content-center mt-5">
                    @foreach($semua as $all)
                    <div class="col-md-3 col-sm-6 col-xs-12 mb-3">
                        <div class="card">
                            <div class="vertical-modal-ex">
                                <a href="" data-bs-toggle="modal" data-bs-target="#exampleModalScrollable-{{ $all->id }}">
                                    <img class="card-img-top" src="{{ asset('app-assets/images/illustration/'.$all->foto.'') }}" alt="Foto Paket"/>  
                                </a>
                                <!-- Modal -->
                                <div class="modal fade" id="exampleModalScrollable-{{ $all->id }}" tabindex="-1" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalScrollableTitle">Unduh Hasil Proyek</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                            @if($all->link_project)
                                                <div class="row">
                                                    <div class="col">
                                                        <img src="{{ asset('app-assets/images/illustration/done.png') }}" class="img-fluid rounded mx-auto d-block"/>
                                                    </div>
                                                    <div class="col">
                                                        <h4 class="py-3">Proyek Anda Telah Selesai dan Siap Diunduh!</h5>
                                                    </div> 
                                                </div>        
                                            @else
                                            <div class="row">
                                                <div class="col">
                                                    <img src="{{ asset('app-assets/images/illustration/in progres.png') }}" class="img-fluid rounded mx-auto d-block py-1"/>   
                                                </div>
                                                <div class="col">
                                                    <h4 class="my-5">Proyek Anda Masih dalam Progres. Silahkan Kembali Lagi Nanti!</h5>
                                                </div> 
                                            </div>
                                            @endif                     
                                            </div>

                                            <div class="modal-footer">
                                                @if($all->link_project && $all->sisa_bayar == 0)
                                                    <a type="button" name="unduh" class="btn btn-primary" href="{{ url($all->link_project) }}">
                                                        <span>Unduh</span>
                                                        <i data-feather='download'></i>
                                                    </a>
                                                @else
                                                    @if($all->link_project && $all->sisa_bayar > 0)
                                                        @php $title = 'Silahkan lunasi sisa pembayaran untuk dapat mengunduh hasil proyek'; @endphp
                                                    @else
                                                        @php $title = 'Proyek masih dalam progres'; @endphp
                                                    @endif
                                                    <span class="d-inline-block" tabindex="0" data-bs-toggle="tooltip" data-bs-placement="bottom" title="{{ $title }}">
                                                            <a type="button" name="unduh2" class="btn btn-primary  disabled" href="#">
                                                                <span>Unduh</span>
                                                                <i data-feather='download'></i>
                                                            </a>
                                                    </span>              
                                                @endif
                                            </div>                                  
                                        </div>
                                    </div>
                                </div>             
                            </div> 
                            <div class="card-body">
                                <a href="" data-bs-toggle="modal" data-bs-target="#exampleModalScrollable-{{ $all->id }}">
                                    <h4 class="card-title">Proyek {{ $all->jenis_paket }}</h4>
                                </a>
                                <div class="d-flex justify-content-between">
                                    <p class="card-text">
                                        Tanggal
                                    </p>
                                    <p class="card-text">
                                        {{ $all->created_at->format('d-m-Y') }}
                                    </p>
                                </div>
                                <!-- <div class="d-flex justify-content-between mt-1">
                                    <p class="card-text" style="font-size: 13px;">
                                    </p>
                                    <p class="card-text" style="font-size: 13px;">
                                        {{ $all->size_project }}
                                    </p>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>

            <!-- Progres-->
            <div class="tab-pane" id="on-going" role="tabpanel" aria-labelledby="on-going-project" aria-expanded="false">
                <div class="row justify-content-center mt-5">
                    @foreach($progress as $progres)
                    <div class="col-md-3 col-sm-6 col-xs-12 mb-3">
                        <div class="card">
                            <div class="vertical-modal-ex">
                                <a href="" data-bs-toggle="modal" data-bs-target="#progres-{{ $progres->id }}">
                                    <img class="card-img-top" src="{{ asset('app-assets/images/illustration/'.$progres->foto.'') }}" alt="Card image cap"/>  
                                </a>
                                <!-- Modal -->
                                <div class="modal fade" id="progres-{{ $progres->id }}" tabindex="-1" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalScrollableTitle">Unduh Hasil Proyek</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                            @if($progres->link_project)
                                                <div class="row">
                                                    <div class="col">
                                                        <img src="{{ asset('app-assets/images/illustration/done.png') }}" class="img-fluid rounded mx-auto d-block"/>
                                                    </div>
                                                    <div class="col">
                                                        <h4 class="py-3">Proyek Anda Telah Selesai dan Siap Diunduh!</h5>
                                                    </div> 
                                                </div>        
                                            @else
                                                <div class="row">
                                                    <div class="col">
                                                        <img src="{{ asset('app-assets/images/illustration/in progres.svg') }}" class="img-fluid rounded mx-auto d-block py-1"/>   
                                                    </div>
                                                    <div class="col">
                                                        <h4 class="my-5">Proyek Anda Masih dalam Progres. Silahkan Kembali Lagi Nanti!</h5>
                                                    </div> 
                                                </div>
                                            @endif                              
                                            </div>
                                    
                                            <div class="modal-footer">
                                                @if($progres->link_project && $progres->sisa_bayar == 0)
                                                    <a type="button" name="unduh" class="btn btn-primary" href="{{ url($all->link_project) }}">
                                                        <span>Unduh</span>
                                                        <i data-feather='download'></i>
                                                    </a>
                                                @else
                                                    @if($progres->link_project && $progres->sisa_bayar > 0)
                                                        @php $title = 'Silahkan lunasi sisa pembayaran untuk dapat mengunduh hasil proyek'; @endphp
                                                    @else
                                                        @php $title = 'Proyek masih dalam progres'; @endphp
                                                    @endif
                                                    <span class="d-inline-block" tabindex="0" data-bs-toggle="tooltip" data-bs-placement="bottom" title="{{ $title }}">
                                                            <a type="button" name="unduh2" class="btn btn-primary  disabled" href="#">
                                                                <span>Unduh</span>
                                                                <i data-feather='download'></i>
                                                            </a>
                                                    </span>              
                                                @endif
                                            </div>                                  
                                        </div>
                                    </div>
                                </div>             
                            </div> 
                            <div class="card-body">
                                <a href="" data-bs-toggle="modal" data-bs-target="#progres-{{ $progres->id }}">
                                    <h4 class="card-title">Proyek {{ $progres->jenis_paket }}</h4>
                                </a>
                                <div class="d-flex justify-content-between">
                                    <p class="card-text">
                                        Tanggal
                                    </p>
                                    <p class="card-text">
                                        {{ $progres->created_at->format('d-m-Y') }}
                                    </p>
                                </div>
                                <!-- <div class="d-flex justify-content-between mt-1">
                                    <p class="card-text" style="font-size: 13px;">
                                    </p>
                                    <p class="card-text" style="font-size: 13px;">
                                        {{ $all->size_project }}
                                    </p>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>

            <!-- Selesai -->
            <div class="tab-pane" id="finished" role="tabpanel" aria-labelledby="finished-project" aria-expanded="false">
                <div class="row justify-content-center mt-5">
                    @foreach($selesai as $done)
                    <div class="col-md-3 col-sm-6 col-xs-12 mb-3">
                        <div class="card">
                            <div class="vertical-modal-ex">
                                <a href="" data-bs-toggle="modal" data-bs-target="#done-{{$done->id}}">
                                    <img class="card-img-top" src="{{ asset('app-assets/images/illustration/'.$done->foto.'') }}" alt="Card image cap"/>  
                                </a>
                                <!-- Modal -->
                                <div class="modal fade" id="done-{{$done->id}}" tabindex="-1" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalScrollableTitle">Unduh Hasil Proyek</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                    
                                            <div class="modal-body">
                                            @if($done->link_project)
                                                <div class="row">
                                                    <div class="col">
                                                        <img src="{{ asset('app-assets/images/illustration/done.png') }}" class="img-fluid rounded mx-auto d-block"/>
                                                    </div>
                                                    <div class="col">
                                                        <h4 class="py-3">Proyek Anda Telah Selesai dan Siap Diunduh!</h5>
                                                    </div> 
                                                </div>        
                                            @else
                                                <div class="row">
                                                    <div class="col">
                                                        <img src="{{ asset('app-assets/images/illustration/in progres.svg') }}" class="img-fluid rounded mx-auto d-block py-1"/>   
                                                    </div>
                                                    <div class="col">
                                                        <h4 class="my-5">Proyek Anda Masih dalam Progres. Silahkan Kembali Lagi Nanti!</h5>
                                                    </div> 
                                                </div>
                                            @endif                 
                                            </div>

                                            <div class="modal-footer">
                                                @if($done->link_project && $done->sisa_bayar == 0)
                                                    <a type="button" name="unduh" class="btn btn-primary" href="{{ url($done->link_project) }}">
                                                        <span>Unduh</span>
                                                        <i data-feather='download'></i>
                                                    </a>
                                                @else
                                                    @if($done->link_project && $done->sisa_bayar > 0)
                                                        @php $title = 'Silahkan lunasi sisa pembayaran untuk dapat mengunduh hasil proyek'; @endphp
                                                    @else
                                                        @php $title = 'Proyek masih dalam progres'; @endphp
                                                    @endif
                                                    <span class="d-inline-block" tabindex="0" data-bs-toggle="tooltip" data-bs-placement="bottom" title="{{ $title }}">
                                                            <a type="button" name="unduh2" class="btn btn-primary  disabled" href="#">
                                                                <span>Unduh</span>
                                                                <i data-feather='download'></i>
                                                            </a>
                                                    </span>              
                                                @endif
                                            </div>                                  
                                        </div>
                                    </div>
                                </div>             
                            </div> 
                            <div class="card-body">
                                <a data-bs-toggle="modal" data-bs-target="#done-{{$done->id}}">
                                    <h4 class="card-title">Proyek {{ $done->jenis_paket }}</h4>
                                </a>
                                <div class="d-flex justify-content-between">
                                    <p class="card-text">
                                        Tanggal
                                    </p>
                                    <p class="card-text">
                                        {{ $done->created_at->format('d-m-Y') }}
                                    </p>
                                </div>
                                <!-- <div class="d-flex justify-content-between mt-1">
                                    <p class="card-text" style="font-size: 13px;">
                                    </p>
                                    <p class="card-text" style="font-size: 13px;">
                                        {{ $all->size_project }}
                                    </p>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
               