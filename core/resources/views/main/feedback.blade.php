@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content">
    <div class="row">
        <div class="col-lg-12 text-center">
        <h1 class="mt-3">Hi, There! 🖐</h1>
            <p class="mt-1 mb-1 pb-75">
                We would like to hear your experience using our services.<br/>
                Please leave us a feedback to let us know what to improve. Thank you!
            </p>
        </div>
    </div>
    <div class="row justify-content-center mt-2">
        <div class="col-8">
            <div class="card">
                <div class="card-body text-center">
                    <div class="d-flex justify-content-between">
                        <img class="img-fluid rounded" src="{{ asset('app-assets/images/pages/feedback.jpg') }}" alt="feedback illustration" width="47%">  
                        <div class="col-6">
                            <form action="{{ route('post-feedback') }}" method="post">
                                @csrf
                                <textarea rows="8" class="form-control" name="feedback" placeholder="Write your feedback here..." autofocus></textarea>
                                <button class="btn btn-primary mt-2" type="submit">
                                    <span>Send Feedback</span>
                                    <i data-feather='send'></i>
                                </button> 
                            </form>                                 
                        </div>       
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

</div>

@endsection