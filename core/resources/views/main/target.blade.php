@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            {{-- <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    
                </div>
            </div> --}}
            <div class="content-body">
                <!-- Basic Tables start -->
                <div class="row" id="basic-table">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Tabel Target</h4>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn" data-bs-toggle="modal" data-bs-target="#defaultSize">
                                    <i data-feather="plus-circle"></i>
                                </button>
                                <!-- Modal -->
                                <div class="modal fade text-start" id="defaultSize" tabindex="-1" aria-labelledby="myModalLabel18" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel18">Pencapaian Target Penjualan</h4>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <form action="{{ route('data_target') }}" method="post">
                                                @csrf
                                                <div class="modal-body">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" name="target" placeholder="" aria-describedby="button-addon2" />
                                                        <button class="btn btn-outline-primary" id="button-addon2" type="submit">Submit</button>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <p>Universe Digilab</p>
                                                    {{-- <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Accept</button> --}}
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                {{-- <a type="submit" class="btn btn-primary" name="submit" value="Submit" href="{{route('form_target')}}" >Buat Target</a> --}}
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Target</td>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            @php
                                            $no = 1;
                                            @endphp

                                            @foreach ($data_target as $target)
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $target->target }}</td>
                                            <td>
                                                <div class="dropdown">
                                                    <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0" data-bs-toggle="dropdown">
                                                        <i data-feather="more-vertical"></i>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-end">
                                                        <a class="dropdown-item" href="{{ url('/target/'.$target->id.'/edit') }}">
                                                            <i data-feather="edit-2" class="me-50"></i>
                                                            <span>Edit</span>
                                                        </a>
                                                        <a class="dropdown-item" href="{{ url('/target/'.$target->id.'/delete') }}">
                                                            <i data-feather="trash" class="me-50"></i>
                                                            <span>Hapus</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                
                            </div>
                            {{-- <div class="card-body">
                                 <p class="card-text">Pagination with icon and text</p> 
                                showing {{ $data_pekerjaan->firstItem() }} to {{ $data_pekerjaan->lastItem() }} of {{ $data_pekerjaan->total() }}
                                <nav aria-label="Page navigation">
                                    <ul class="pagination mt-1">
                                        {{ $data_pekerjaan->links() }} 
                                        
                                    </ul>
                                </nav>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <!-- Basic Tables end -->
            </div>
        </div>
    </div>
@endsection