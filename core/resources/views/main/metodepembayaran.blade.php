@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            {{-- <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    
                </div>
            </div> --}}
            <div class="content-body">
                <!-- Basic Tables start -->
                <div class="row" id="basic-table">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Tabel Metode Pembayaran</h4>
                                <a type="submit" class="btn btn-primary" name="submit" value="Submit" href="{{route('formmetodepembayaran')}}" >Tambah Metode Pembayaran</a>
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Pemilik Rekening</th>
                                            <th>Metode Pembayaran</th>
                                            <th>Nama Provider</th>
                                            <th>Foto Provider</th>
                                            <th>No Rekening</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            {{-- @php
                                            $no = 1;
                                            @endphp --}}

                                            @foreach ($data_metode_pembayaran as $key => $dmp)
                                            <td>{{ $data_metode_pembayaran->firstItem() + $key }}</td>
                                            <td>{{ $dmp->nama_pemilik_rekening }}</td>
                                            <td>{{ $dmp->metode_pembayaran }}</td>
                                            <td>{{ $dmp->nama_provider}}</td>
                                            <td>
                                                <img src="{{ asset('app-assets/images/pages/eCommerce/'.$dmp->foto_provider)}}" class="img-thumbnail" alt="{{ $dmp->nama_provider}}" style="width: 5cm;">
                                                {{-- <a href="{{ asset('app-assets/images/pages/eCommerce/'.$dmp->foto_provider)}}" target="_blank" alt="{{ $dmp->nama_provider}}" style="width: 5cm;">Lihat Gambar Provider</a> --}}
                                            </td>
                                            <td>{{ $dmp->no_rekening}}</td>
                                            <td>
                                                <div class="dropdown">
                                                    <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0" data-bs-toggle="dropdown">
                                                        <i data-feather="more-vertical"></i>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-end">
                                                        <a class="dropdown-item" href="{{ url('/metodepembayaran/'.$dmp->id.'/edit') }}">
                                                            <i data-feather="edit-2" class="me-50"></i>
                                                            <span>Edit</span>
                                                        </a>
                                                        <a class="dropdown-item" href="{{ url('/metodepembayaran/'.$dmp->id.'/delete') }}">
                                                            <i data-feather="trash" class="me-50"></i>
                                                            <span>Hapus</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                
                            </div>
                            <hr>
                            <div class="card-body">
                                {{-- <p class="card-text">Pagination with icon and text</p> --}}
                                showing {{ $data_metode_pembayaran->firstItem() }} to {{ $data_metode_pembayaran->lastItem() }} of {{ $data_metode_pembayaran->total() }}
                                <nav aria-label="Page navigation">
                                    <ul class="pagination mt-1">
                                        {{ $data_metode_pembayaran->links() }} 
                                        
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Basic Tables end -->
            </div>
        </div>
    </div>
@endsection