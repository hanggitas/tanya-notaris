@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            {{-- <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    
                </div>
            </div> --}}
            <div class="content-body">
                <!-- Basic Tables start -->
                <div class="row" id="basic-table">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Tabel Kontak</h4>
                                <a type="submit" class="btn btn-primary" name="submit" value="Submit" href="{{route('formkontak')}}" >Tambah Kontak</a>
                            </div>
                         
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Email</th>
                                            <th>Nomor Telepon</th>
                                            <th>Alamat</th>
                                            <th>Logo</th>
                                            <th>Instagram</th>
                                            <th>Youtube</th>
                                            <th>LinkedIn</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            {{-- @php
                                            $no = 1;
                                            @endphp --}}

                                            @foreach ($data_kontak as $key => $kontak)
                                            <td> {{ $data_kontak->firstItem() + $key }}</td>
                                            <td> {{ $kontak-> email }}</td>
                                            <td> 0{{ $kontak-> nomor_telepon }} </td>
                                            <td> {{ $kontak-> alamat }} </td>
                                            <td>
                                                <img src="{{ asset('app-assets/images/logo/'.$kontak->logo) }}" class="" alt="logo" style="width: 5cm;">
                                                {{-- <a href="{{ asset('app-assets/images/logo/'.$kontak->logo) }}" target="_blank" alt="logo" style="width: 5cm;">Lihat Gambar</a> --}}
                                            </td>
                                            <td> {{ $kontak-> instagram}}</td>
                                            <td> {{ $kontak-> youtube }}</td>
                                            <td> {{ $kontak-> linkedin }}</td>
                                            <td>
                                                <div class="dropdown">
                                                    <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0" data-bs-toggle="dropdown">
                                                        <i data-feather="more-vertical"></i>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-end">
                                                        <a class="dropdown-item" href="{{ url('/kontak/'.$kontak->id.'/edit') }}">
                                                            <i data-feather="edit-2" class="me-50"></i>
                                                            <span>Edit</span>
                                                        </a>
                                                        <a class="dropdown-item" href="{{ url('/kontak/'.$kontak->id.'/delete') }}">
                                                            <i data-feather="trash" class="me-50"></i>
                                                            <span>Hapus</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>    
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                            <hr>
                            <div class="card-body">
                                {{-- <p class="card-text">Pagination with icon and text</p> --}}
                                showing {{ $data_kontak->firstItem() }} to {{ $data_kontak->lastItem() }} of {{ $data_kontak->total() }}
                                <nav aria-label="Page navigation">
                                    <ul class="pagination mt-1">
                                        {{ $data_kontak->links() }} 
                                        
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Basic Tables end -->

               

            </div>
        </div>
    </div>
@endsection