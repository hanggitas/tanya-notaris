@extends('main.katalogdashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('main.content')
@extends('main.footer')
@section('content')
  <!-- BEGIN: Content-->
    <div class="app-content content ecommerce-application">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
                <div class="content-body">
                <div class="text-center">
                        <h1 class="mt-5">Katalog</h1>
                        <p class="mb-2 pb-75">
                            Temukan berbagai jenis paket. Pilih paket terbaik yang sesuai dengan kebutuhan anda. 
                        </p>
                </div>
                <h4 class="mt-2">Cari berdasarkan Kategori</h4>
                    <!-- E-commerce Content Section Starts -->
             
                    <!-- E-commerce Content Section Starts -->

                    <!-- background Overlay when sidebar is shown  starts-->
                    <div class="body-content-overlay"></div>
                    <!-- background Overlay when sidebar is shown  ends-->

                    <!-- E-commerce Search Bar Starts -->
                    <!-- E-commerce Search Bar Ends -->
                    <!-- E-commerce Products Starts -->
                    <section id="ecommerce-products" class="grid-view">
                        @foreach ($kategori as $item)
                        <div class="card ecommerce-card p-2 ps-0">
                            <div class="card-body">
                                <h6 class="item-name">
                                   <a class="text-body text-left" href="{{ url('/katalog/detail-katalog/'.$item->id.'') }}">{{ $item->nama_kategori }}
                                   <i data-feather='chevron-right'></i>
                                   </a>
                                </h6>
                            </div>
                        </div> 
                        @endforeach                    
                    </section>
                    <!-- E-commerce Products Ends -->

                    <!-- E-commerce Pagination Starts -->
                    <section id="ecommerce-pagination">
                        <div class="row">
                            <div class="col-sm-12">
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination justify-content-center mt-2">
                                        <li class="page-item prev-item"><a class="page-link" href="#"></a></li>
                                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item" aria-current="page"><a class="page-link" href="#">4</a></li>
                                        <li class="page-item"><a class="page-link" href="#">5</a></li>
                                        <li class="page-item"><a class="page-link" href="#">6</a></li>
                                        <li class="page-item"><a class="page-link" href="#">7</a></li>
                                        <li class="page-item next-item"><a class="page-link" href="#"></a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </section>
                    <!-- E-commerce Pagination Ends -->

                </div>  
        </div>
    </div>
    <!-- END: Content-->
@endsection
