@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            {{-- <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    
                </div>
            </div> --}}
            <div class="content-body">
                <!-- Basic Tables start -->
                <div class="row" id="basic-table">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Tabel Pekerjaan</h4>
                                {{-- <a type="submit" class="btn btn-primary" name="submit" value="Submit" href="{{route('form_pekerjaan')}}" >Tambah Pekerjaan</a> --}}
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>ID Pekerjaan</td>
                                            <th>ID Order</th>
                                            <th>Pre-Produksi</th>
                                            <th>Produksi</th>
                                            <th>Post-Produksi</th>
                                            <th>Selesai</th>
                                            <th>Link Projek</th>
                                            <th>Size Projek</th>
                                            <th>Diunggah</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            {{-- @php
                                            $no = 1;
                                            @endphp --}}

                                            @foreach ($data_pekerjaan as $key => $kerja)
                                            <td>{{ $data_pekerjaan->firstItem() + $key }}</td>
                                            <td>{{ $kerja->id }}</td>
                                            <td>{{ $kerja->id_order }}</td>
                                            {{-- <td>{{ $kerja->name }}</td> --}}
                                            <td>{{ $kerja->pre_produksi }}</td>
                                            <td>{{ $kerja->produksi }}</td>
                                            <td>{{ $kerja->post_produksi }}</td>
                                            <td>{{ $kerja->selesai }}</td>
                                            <td>{{ $kerja->link_project }}</td>
                                            <td>{{ $kerja->size_project }}</td>
                                            <td>{{ $kerja->diunggah }}</td>
                                            <td>
                                                <div class="dropdown">
                                                    <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0" data-bs-toggle="dropdown">
                                                        <i data-feather="more-vertical"></i>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-end">
                                                        <a class="dropdown-item" href="{{ url('/pekerjaan/'.$kerja->id.'/edit') }}">
                                                            <i data-feather="edit-2" class="me-50"></i>
                                                            <span>Edit</span>
                                                        </a>
                                                        <a class="dropdown-item" href="{{ url('/pekerjaan/'.$kerja->id.'/delete') }}">
                                                            <i data-feather="trash" class="me-50"></i>
                                                            <span>Hapus</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                
                            </div>
                            <div class="card-body">
                                {{-- <p class="card-text">Pagination with icon and text</p> --}}
                                showing {{ $data_pekerjaan->firstItem() }} to {{ $data_pekerjaan->lastItem() }} of {{ $data_pekerjaan->total() }}
                                <nav aria-label="Page navigation">
                                    <ul class="pagination mt-1">
                                        {{ $data_pekerjaan->links() }} 
                                        
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Basic Tables end -->
            </div>
        </div>
    </div>
@endsection