@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            {{-- <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    
                </div>
            </div> --}}
            <div class="content-body">
                <!-- Basic Tables start -->
                <div class="row" id="basic-table">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Tabel Order</h4>
                                {{-- <a href="{{ route('tambah_data_portofolio') }}" class="btn btn-primary">Tambah Portofolio</a> --}}
                                
                            </div>
                         
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Tipe Order</th>
                                            <th>Nama User</th>
                                            <th>Metode Pembayaran</th>
                                            <th>ID Service Subkategori</th>
                                            <th>Bukti Pembayaran DP</th>
                                            <th>Bukti Pembayaran Lunas</th>
                                            <th>Status Pembayaran</th>
                                            <th>Waktu</th>
                                            <th>Total Harga</th>
                                            {{-- <th>Total Dibayar</th> --}}
                                            <th>Total Bayar DP</th>
                                            <th>Total Bayar Lunas</th>
                                            <th>Sisa Bayar</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            {{-- @php
                                            $no=1;
                                            @endphp --}}

                                            @foreach ($orders as $key => $order)
                                            <div class="form-modal-ex">
                                                <!-- Button trigger modal -->
                                                {{-- <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#inlineForm">
                                                    Buat Order Paket Custom
                                                </button> --}}
                                                <!-- Modal -->
                                                <div class="modal fade text-start" id="inlineForm{{ $order->id }}" tabindex="-1" aria-labelledby="myModalLabel33" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myModalLabel33">Masukkan Paket Custom</h4>
                                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <form action="{{ route('custom_order') }}" method="post">
                                                                {{ csrf_field() }}
                                                                <div class="modal-body">
                                                                    <label>Kategori </label>
                                                                    <div class="mb-1">
                                                                        <select class="select2 form-select" id="select2-basic" name="id_kategori">
                                                                            <option value="">-- Pilih Kategori --</option>
                                                                            @foreach($pilihan_kategori as $pilih_kategori)
                                                                            
                                                                            <option value="{{ $pilih_kategori->id }}">
                                                                                {{ $pilih_kategori->nama_kategori }}
                                                                            </option>
                                                                            
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
            
                                                                    <label>Subkategori </label>
                                                                    <div class="mb-1">
                                                                        <select class="select2 form-select" id="select2-basic" name="id_subkategori">
                                                                            <option value="">-- Pilih Subkategori --</option>
                                                                            @foreach($pilihan_subkategori as $pilih_subKat)
                                                                            
                                                                            <option value="{{ $pilih_subKat->id }}">
                                                                                {{ $pilih_subKat->nama_subkategori }}
                                                                            </option>
                                                                            
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
            
                                                                    <label>Jenis Paket </label>
                                                                    <div class="mb-1">
                                                                        <input type="text" name="jenis_paket" placeholder="" class="form-control" />
                                                                    </div>
            
                                                                    <label>Detail Paket </label>
                                                                    <div class="mb-1">
                                                                        <input type="text" name="detail_paket" placeholder="" class="form-control" />
                                                                    </div>
            
                                                                    <label>Total Harga </label>
                                                                    <div class="mb-1">
                                                                        <input type="text" name="harga" placeholder="" class="form-control" />
                                                                    </div>
                                                                    
                                                                    <input type="hidden" name="id_order" placeholder="" value="{{ $order->id }}" class="form-control" />
                                                                    
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="submit" class="btn btn-primary" data-bs-dismiss="modal">Submit</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <td>{{ $orders->firstItem() + $key}}</td>
                                            <td>{{ $order->tipe_order }}</td>
                                            <td>{{ $order->name }}</td>
                                            <td>{{ $order->metode_pembayaran }}</td>
                                            <td>
                                                @if($order->sersub_id == NULL)
                                                    @if($order->sersub_id != NULL)
                                                    {{ $order->sersub_id }}
                                                    @else
                                                    <div class="vertical-modal-ex">
                                                        <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#inlineForm{{ $order->id }}">
                                                            Buat
                                                        </button>
                                                    </div>
                                                    @endif
                                                @else
                                                    <a href="" data-bs-toggle="modal" data-bs-target="#sersub-{{ $order->id }}"> Detail Paket</a>
                                                    <!-- Modal -->
                                                    <div class="modal fade" id="sersub-{{$order->id}}" tabindex="-1" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalScrollableTitle">Paket Milik {{ $order->name }}</h5>
                                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    @php
                                                                    
                                                                    $detail =  App\Models\service_subkategori::leftjoin('kategori', 'service_subkategori.id_kategori', '=', 'kategori.id')
                                                                    ->leftjoin('subkategori','service_subkategori.id_subkategori', '=', 'subkategori.id')
                                                                    ->leftjoin('services', 'service_subkategori.id_services', '=', 'services.id')            
                                                                    ->select('kategori.nama_kategori', 'subkategori.nama_subkategori', 'services.jenis_paket')
                                                                    ->groupBy('service_subkategori.id')
                                                                    ->where('service_subkategori.id',$order->sersub_id)
                                                                    ->get();

                                                                    // $detail1 =  App\Models\service_subkategori::leftjoin('kategori', 'service_subkategori.id_kategori', '=', 'kategori.id')
                                                                    // ->leftjoin('subkategori','service_subkategori.id_subkategori', '=', 'subkategori.id')
                                                                    // ->leftjoin('services', 'service_subkategori.id_services', '=', 'services.id')            
                                                                    // ->select('kategori.nama_kategori', 'subkategori.nama_subkategori', 'services.jenis_paket')
                                                                    // ->groupBy('service_subkategori.id')
                                                                    // ->where('service_subkategori.id',$order->sersub_id)
                                                                    // ->get();
                                                                    @endphp
                                                                @if($detail[0]['nama_subkategori'] != NULL)
                                                                <p> Kategori    : {{ $detail[0]['nama_kategori'] }} </p>
                                                                <p> Subkategori : {{ $detail[0]['nama_subkategori'] }} </p>
                                                                <p> Nama Paket  : {{ $detail[0]['jenis_paket'] }} </p>
                                                                @else
                                                                <p> Kategori    : {{ $detail[0]['nama_kategori'] }} </p>
                                                                <p> Nama Paket  : {{ $detail[0]['jenis_paket'] }} </p>
                                                                @endif
                                                                    {{-- {{ $detail }}           --}}
                                                                </div>                                   
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{-- {{ $order->sersub_id }} --}}
                                                @endif
                                            </td>
                                            {{-- <td>
                                                @foreach($ord  as $orde)
                                                {{ $orde->nama_kategori }}
                                                @endforeach
                                            </td> --}}
                                            <td>
                                                @if($order->tipe_order == 'custom')
                                                    @if($order->bukti_bayar_dp)
                                                {{-- <img src="{{ $order->bukti_bayar_dp }}" alt="" class="img-thumbnail" style="width: 10cm"> --}}
                                                    {{-- <a href="{{ $order->bukti_bayar_dp }}" target="_blank" alt="" style="width: 5cm;">Lihat Bukti DP</a> --}}
                                                <div class="vertical-modal-ex">
                                                    <a href="" data-bs-toggle="modal" data-bs-target="#dp-{{ $order->id }}">Lihat Bukti DP</a>
                                                    <!-- Modal -->
                                                    <div class="modal fade" id="dp-{{$order->id}}" tabindex="-1" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalScrollableTitle">Preview bukti bayar DP</h5>
                                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <img src="{{ $order->bukti_bayar_dp }}" class="img-fluid rounded mx-auto d-block"/>                     
                                                                </div>                                   
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                    @endif
                                                @else
                                                <p>Hanya untuk paket custom</p>
                                                @endif
                                            </td>
                                            <td>
                                                @if($order->bukti_bayar_lunas)
                                                    {{-- <a href="{{ $order->bukti_bayar_lunas }}" target="_blank" alt="" style="width: 5cm;">Lihat Bukti Pelunasan</a> --}}
                                                <div class="vertical-modal-ex">
                                                    <a href="" data-bs-toggle="modal" data-bs-target="#lunas-{{ $order->id }}">Lihat Bukti Pelunasan</a>
                                                    <div class="modal fade" id="lunas-{{$order->id}}" tabindex="-1" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalScrollableTitle">Preview bukti bayar lunas</h5>
                                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <img src="{{ $order->bukti_bayar_lunas }}" class="img-fluid rounded mx-auto d-block"/>                     
                                                                </div>                                   
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                            </td>
                                            <td>{{ $order->status_pembayaran }}</td>
                                            <td>{{ $order->date_time }}</td>
                                            <td>
                                                @if($order->total_harga)
                                                Rp{{ number_format($order->total_harga, 0, '', '.') }}
                                                @endif
                                            </td>
                                            {{-- <td>{{ $order->total_dibayar }}</td> --}}
                                            <td>
                                                @if($order->total_bayar_dp)
                                                Rp{{ number_format($order->total_bayar_dp, 0, '', '.') }}
                                                @else
                                                <p> Hanya untuk paket custom </p>
                                                @endif
                                            </td>
                                            <td>
                                                @if($order->tipe_order == "custom")
                                                    @if($order->status_pembayaran == "Lunas")
                                                    Rp{{ number_format($order->total_harga, 0, '', '.')  }}
                                                    @endif
                                                @else
                                                <p> Hanya untuk paket custom </p>
                                                @endif
                                            </td>
                                            <td>
                                                @if($order->tipe_order == "custom")
                                                    @if($order->status_pembayaran == "Belum Lunas")
                                                    Rp{{ number_format($order->total_bayar_dp, 0, '', '.') }}
                                                    @else
                                                    0
                                                    @endif
                                                @else
                                                <p> Hanya untuk paket custom </p>
                                                @endif
                                            </td>
                                            <td>
                                                <div class="dropdown">
                                                    <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0" data-bs-toggle="dropdown">
                                                        <i data-feather="more-vertical"></i>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-end">
                                                        <a class="dropdown-item" href="{{ url('edit-order/'.$order->id) }}">
                                                            <i data-feather="edit-2" class="me-50"></i>
                                                            <span>Edit</span>
                                                        </a>
                                                        <a class="dropdown-item" href="{{ url('delete-order/'.$order->id) }}">
                                                            <i data-feather="trash" class="me-50"></i>
                                                            <span>Delete</span>
                                                        </a>
                                                        {{-- @if($order->id_service_subkategori == NULL) --}}
                                                        
                                                        {{-- <a class="dropdown-item" id="inlineForm" tabindex="-1" aria-labelledby="myModalLabel33" aria-hidden="true" href="#">
                                                            <i data-feather='plus'></i>
                                                            <span type="button" data-bs-toggle="modal" data-bs-target="{{'inlineForm'.$order->id}}">
                                                                Buat Order Paket Custom
                                                            </span>
                                                            {{-- <span id="myModalLabel33">Edit Pesanan</span>
                                                        </a>
                                                        @endif --}}
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                
                            </div>
                            
                            
                            <div class="card-body">
                                {{-- <p class="card-text">Pagination with icon and text</p> --}}
                                showing {{ $orders->firstItem() }} to {{ $orders->lastItem() }} of {{ $orders->total() }}
                                <nav aria-label="Page navigation">
                                    <ul class="pagination mt-1">
                                        {{ $orders->links() }} 
                                        
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Basic Tables end -->

               

            </div>
        </div>
    </div>

    {{-- @include('form.modal_custom') --}}
@endsection