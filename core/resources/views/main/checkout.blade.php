@extends('main.checkoutdashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ecommerce-application">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Checkout</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Katalog</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{ route('detailklien') }}">Layanan</a>
                                </li>
                                <li class="breadcrumb-item active">Checkout
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <section id="pricing-plan">
                    <!-- title text and switch button -->
                    <div class="text-center">
                        <h1 class="mt-3">Checkout Paket</h1>
                        <p class="mb-0 pb-75">
                            Checkout paket
                        </p>
                    </div>
        </section>
        <div class="content-body">
            <div class="bs-stepper checkout-tab-steps">
                <!-- Wizard starts -->
                <div class="bs-stepper-header">
                    <div class="step" data-target="#step-cart" role="tab" id="step-cart-trigger">
                        <button type="button" class="step-trigger">
                </div>
                <!-- Wizard ends -->

                <div class="bs-stepper-content">
                    <!-- Checkout Place order starts -->
                    <div id="step-cart" class="content" role="tabpanel" aria-labelledby="step-cart-trigger">
                        <div id="place-order" class="list-view product-checkout">
                            
                            <!-- Checkout Place Order Left starts -->
                            <div class="checkout-items">
                            <div class="row justify-content-center">
                            <div class="col-lg-6">
                                <div class="card">
                                    <img class="card-img-top" src="{{ asset('app-assets/images/pages/eCommerce/1.png') }}" alt="Card image cap" />
                                    <div class="card-body">
                                        <div class="item-name">
                                            <h4 class="card-title"><a href="">{{ $services['jenis_paket']}}</a></h4>
                                            <div class="item-rating">
                                                <ul class="unstyled-list list-inline">
                                                    <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                    <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                    <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                    <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                    <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <h5 class="text-success">Rp{{ number_format($services['harga'], 0, '', '.') }}</h5> 
                                    </div>
                                </div>
                            </div>
                            </div>
                            </div>
                            <!-- Checkout Place Order Left ends -->
                            
                            <!-- Checkout Place Order Right starts -->
                           <div class="checkout-options">
                                <div class="card">
                                    <div class="card-body">
                                        <label class="section-label form-label mb-1">Detail Pemesanan</label>
                                        <hr/>
                                        <div class="price-details">
                                            <h6 class="price-title">Detail Item</h6>
                                            <ul class="list-unstyled">
                                                <li class="price-detail">
                                                    <div class="detail-title">Jenis Paket</div>
                                                    <div class="detail-amt">{{ $services['jenis_paket']}}</div>
                                                </li>
                                                <li class="price-detail">
                                                    <div class="detail-title">Harga</div>
                                                    <div class="detail-amt">Rp{{ number_format($services['harga'], 0, '', '.') }}</div>
                                                </li>
                                            <form action="{{ route('bayar') }}" method="post">
                                            @csrf
                                                <input type="hidden" name="tipe_order" value="reguler" />
                                                <input type="hidden" name="sisa_bayar" value="0" />
                                                <input type="hidden" name="total_dibayar" value="{{ $services['harga'] }}" />
                                                <input type="hidden" name="status_pembayaran" value="Berhasil" />
                                                <li class="price-detail">
                                                    <div class="detail-title">Metode <br>Pembayaran</div>
                                                    <div class="detail-amt">
                                                        <input class="form-check-input" type="radio" name="metode_bayar" value="{{ $metode[0]['id'] }}" required/>
                                                        <label>{{ $metode[0]['metode_pembayaran'] }}</label>
                                                    </div>
                                                </li>
                                            <hr/>
                                                <li class="price-detail">
                                                    <div class="detail-title detail-total">Total</div>
                                                    <div class="detail-amt fw-bolder">Rp{{ number_format($services['harga'], 0, '', '.') }}</div>
                                                </li>                 
                                            </ul>
                                            <hr />
                                            <h6 class="price-title">Transfer Pembayaran</h6>
                                            <ul class="list-unstyled">
                                            <li class="price-detail">
                                                    <div class="detail-title">
                                                        <img src="{{ asset('app-assets/images/pages/eCommerce/'.$metode[0]['foto_provider'].'') }}" class="img-fluid" style="width:100px" />
                                                    </div>
                                                </li>
                                                <li class="price-detail">
                                                    <div class="detail-title">{{ $metode[0]['nama_pemilik_rekening']}}</div>
                                                </li>
                                                <li class="price-detail">
                                                    <div class="detail-amt fw-bolder">{{ $metode[0]['no_rekening']}}</div>
                                                </li>
                                            
                                           <!-- Mengirimkan nilai -->
                                            <input type="hidden" name="id_service_sub" value=" {{ $services['id_service_sub'] }}">
                                            <input type="hidden" name="harga" value="{{ $services['harga'] }}">
                                            <input type="hidden" name="id_service" value="{{ $services['id'] }}">
                                            <button type="submit" class="btn btn-primary w-100">Beli Paket</button>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- Checkout Place Order Right ends -->
                            </div>
                        </div>
                        <!-- Checkout Place order Ends -->
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection