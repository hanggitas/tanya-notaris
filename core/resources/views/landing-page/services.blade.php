@extends('landing-page.index')
@section('konten')

<!-- Start Consultation 
============================================= -->
    <div class="consultation-area default-padding-bottom">
        <div class="container">
            <div class="row align-center">
                <div class="col-lg-5 form">
                    <div class="appoinment-box text-center wow fadeInRight">
                        <div class="heading">
                            <h4>Paket A</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="consultation-area default-padding-bottom">
        <div class="container">
            <div class="row align-center">
                <div class="col-lg-5 form">
                    <div class="appoinment-box text-center wow fadeInRight">
                        <div class="heading">
                            <h4>Paket B</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="consultation-area default-padding-bottom">
        <div class="container">
            <div class="row align-center">
                <div class="col-lg-5 form">
                    <div class="appoinment-box text-center wow fadeInRight">
                        <div class="heading">
                            <h4>Paket C</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection
