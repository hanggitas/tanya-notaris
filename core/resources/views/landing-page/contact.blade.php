@extends('landing-page.index')
@section('konten')
   <!-- Start Contact Area  
    ============================================= -->
    <div id="contact" class="contact-us-area default-padding">
        <div class="container">
            <div class="contact-items">
                <div class="row">

                    <div class="col-lg-7 wow address-box fadeInUp bg-cover" style="background-image: url(assets/img/2440x1578.png);">
                        <div class="address-info">
                            <ul>
                                <li>
                                    <h5><i class="flaticon-call"></i> Phone</h5>
                                    <span>+62813 1930 3492</span>
                                </li>
                                <li>
                                    <h5><i class="flaticon-email"></i> Email</h5>
                                    <span>semestainovasinusantara@gmail.com</span>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-5 wow fadeInLeft contact-form-box">
                        <h2>Need help? <strong>Let's ask your questions</strong></h2>
                        <form action="assets/mail/contact.php" method="POST" class="contact-form">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input class="form-control" id="name" name="name" placeholder="Name" type="text">
                                        <span class="alert-error"></span>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input class="form-control" id="email" name="email" placeholder="Email*" type="email">
                                        <span class="alert-error"></span>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input class="form-control" id="phone" name="phone" placeholder="Phone" type="text">
                                        <span class="alert-error"></span>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group comments">
                                        <textarea class="form-control" id="comments" name="comments" placeholder="Tell Us About Project *"></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <button type="submit" name="submit" id="submit">
                                        Send Message
                                    </button>
                                </div>
                                <!-- Alert Message -->
                                <div class="col-lg-12 alert-notification">
                                    <div id="message" class="alert-msg"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <br><br><br>
                <div class="row">
                    <div class="col-lg-12 d-flex justify-content-center">
                        <div class="mapouter">
                            <div class="gmap_canvas">
                                <iframe width="1200" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=pt.%20semesta%20inovasi%20nusantara&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
                                </iframe>
                                <a href="https://123movies-to.org">123movies</a>
                                <br>
                                <style>.mapouter{position:relative;text-align:right;height:500px;width:1200px;}</style>
                                <a href="https://www.embedgooglemap.net">embedgooglemap.net</a>
                                <style>.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:1200px;}
                                </style>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>

   
    <!-- End Contact -->

    <!-- Start Google Maps 
@endsection