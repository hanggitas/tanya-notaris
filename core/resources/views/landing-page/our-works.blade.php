@extends('landing-page.index')
@section('konten')
<!-- Start Team 
============================================= -->
    <div class="team-default-area bg-gray default-padding bottom-less">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="site-heading text-center">
                        <h4>OUR WORKS</h4>
                        <h2>OUR WORKS</h2>
                        <p>
                            Take a look at some of our portfolios. we have helped many projects achieved success and deliver the best experience for them
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="team-items text-center">
                <div class="row">
                    <!-- Single Item -->
                    <div class="single-item col-lg-3 col-md-6">
                        <div class="item">
                            <div class="thumb">
                                <img src="{{ asset('assets/img/800x800.png')}}" alt="Thumb">
                                <!-- <div class="contact">
                                    <ul>
                                        <li>
                                            <a href="#"><i class="fas fa-phone"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fas fa-comments"></i></a>
                                        </li>
                                    </ul>
                                </div> -->
                            </div>
                            <div class="info">
                                <h5>Dokumenter PT def</h5>
                                <span>Video sejak tahun...</span>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Item -->
                    <!-- Single Item -->
                    <div class="single-item col-lg-3 col-md-6">
                        <div class="item">
                            <div class="thumb">
                                <img src="{{ asset('assets/img/800x800.png')}}" alt="Thumb">
                            </div>
                            <div class="info">
                                <h5>Photography Makanan</h5>
                                <span>Photo dari UMKM...</span>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Item -->
                    <!-- Single Item -->
                    <div class="single-item col-lg-3 col-md-6">
                        <div class="item">
                            <div class="thumb">
                                <img src="{{ asset('assets/img/800x800.png')}}" alt="Thumb">
                            </div>
                            <div class="info">
                                <h5>Feed IG UKM xyz</h5>
                                <span>Berbagai foto yang digabungkan ...</span>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Item -->
                    <!-- Single Item -->
                    <div class="single-item col-lg-3 col-md-6">
                        <div class="item">
                            <div class="thumb">
                                <img src="{{asset ('assets/img/800x800.png')}}" alt="Thumb">
                            </div>
                            <div class="info">
                                <h5>Video PT xyz</h5>
                                <span>video ini merupakan</span>
                            </div>
                        </div>
                    </div>
                    <!-- End Single Item -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Team -->

    @endsection