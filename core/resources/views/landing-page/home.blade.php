@extends('landing-page.index')
@section('konten')
 
   <!-- Start Banner 
    ============================================= -->
    <div class="banner-area inc-shape content-less">
        <div id="bootcarousel" class="carousel text-light text-large slide carousel-fade animate_text" data-ride="carousel">

            <!-- Wrapper for slides -->
            <div class="carousel-inner carousel-zoom">
                <div class="carousel-item active">
                    <div class="slider-thumb bg-cover" style="background-image: url('{{asset('assets/img/2440x1578.png')}}');"></div>
                    <div class="box-table">
                        <div class="box-cell shadow dark">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-9">
                                        <div class="content">
                                            <h4 data-animation="animated slideInDown">Good doctor, Healthy life</h4>
                                            <h2 data-animation="animated slideInRight">Meet the <strong>Best Doctors</strong></h2>
                                            <a data-animation="animated fadeInUp" class="btn btn-md btn-gradient" href="#"><i class="fas fa-angle-right"></i> Discover More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="slider-thumb bg-cover" style="background-image: url('{{asset('assets/img/2440x1578.png')}}');"></div>
                    <div class="box-table">
                        <div class="box-cell shadow dark">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-9">
                                        <div class="content">
                                            <h4 data-animation="animated slideInDown">Best institution, Good services</h4>
                                            <h2 data-animation="animated slideInRight">Meet the <strong>Best Hospital</strong></h2>
                                            <a data-animation="animated fadeInUp" class="btn btn-md btn-gradient" href="#"><i class="fas fa-angle-right"></i> Discover More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Wrapper for slides -->

            <!-- Left and right controls -->
            <a class="left carousel-control light" href="#bootcarousel" data-slide="prev">
                <i class="fa fa-angle-left"></i>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control light" href="#bootcarousel" data-slide="next">
                <i class="fa fa-angle-right"></i>
                <span class="sr-only">Next</span>
            </a>

        </div>
    </div>
    <!-- End Banner -->

    <!-- Start About
    ============================================= -->
    <div class="about-area default-padding bg-gray relative">

         <!-- Shape -->
        <div class="shape-left-top shape">
            <img src="{{asset('assets/img/shape/1.png')}}" alt="Shape">
        </div>
        <!-- End Shape -->

        <div class="container">
            <div class="row">

                <div class="col-lg-6 thumb">
                    <div class="thumb-box">
                        <img src="{{asset('assets/img/logoaboutus.png')}}" alt="Thumb">
                        <div class="intro-video">
                            <div class="video">
                                <a href="https://www.youtube.com" class="popup-youtube relative theme video-play-button item-center">
                                    <i class="fa fa-play"></i>
                                </a>
                            </div>
                            <div class="content">
                                <h5>Let's see our intro video</h5>
                                <p>
                                    If your smile is not becoming to you, then you should be coming.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 info">
                    <h5>About Us</h5>
                    <h2><strong> About Us </strong> </h2>
                    <p>
                        Pursuit chamber as elderly amongst on. Distant however warrant farther to of. My justice wishing prudent waiting in be. Who decisively attachment has dispatched. Fruit defer in party me built under first. Forbade him but savings sending ham general.
                    </p>
                    <!-- <ul>
                        <li>
                            <div class="icon">
                                <i class="flaticon-calendar"></i>
                            </div>
                            <div class="content">
                                <h5><a href="#">Online Appoinment</a></h5>
                            </div>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="flaticon-drugs"></i>
                            </div>
                            <div class="content">
                                <h5><a href="#">Health Queries</a></h5>
                            </div>
                        </li>
                    </ul> -->
                    <!-- <a class="btn btn-md btn-gradient" href="#"><i class="fas fa-angle-right"></i> Make Appoinment</a> -->
                </div>

            </div>
        </div>
    </div>
    <!-- End About -->

     <!-- Start Why Us
    ============================================= -->
    <div class="choose-us-area">
        <div class="row">
            <div class="col-lg-6 thumb bg-cover" style="background-image: url('{{asset('assets/img/2440x1578.png')}}');"></div>
            <div class="col-lg-6 info">
                <div class="info-box">
                    <h5>Why Us</h5>
                    <h2>Why Us</h2>
                    <p>
                    We’re ready to create amazing things. Delivered from professional and experienced at their respective fields at competitive price. We guaranteed a swift and eazy project life cycle by using customer focus approached and design thinking method.
                    </p>
                    <p>
                    We hold to our Company values, which are #TumbuhBerarti means we ensured the impact always achieved and  #PositifKreatif means guaranteed sustainability received by clients.
                    </p>
                    <!-- <a class="btn btn-md btn-gradient" href="#"><i class="fas fa-angle-right"></i> Doctor Lists</a> -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Why Us -->


    <!-- Start Services 
    ============================================= -->
    <div class="department-area carousel-shadow default-padding-bottom bg-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="site-heading text-center">
                        <br><br><br>
                        <h4>Order Flow</h4>
                        <h2>Order Flow</h2>
                        <!-- <p>
                            Terdapat 2 order flow yakni ...
                        </p> -->
                        <br>
                        <img src="{{asset('assets/img/order_flow.png')}}" alt="Thumb">
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="department-items department-carousel owl-carousel owl-theme">
                        <!-- Single Item -->
                        <!-- <div class="item">
                            <div class="thumb">
                                <img src="{{asset('assets/img/800x600.png')}}" alt="Thumb">
                            </div>
                            <div class="info">
                                <h4><a href="#">Eye Care</a></h4>
                                <p>
                                    Sudden up my excuse to suffer ladies though or. Bachelor possible marianne one directly confined the mention process.  
                                </p>
                                <div class="head-of">
                                    <p>
                                        <strong>Department head: </strong> Prof. Jonathom Doe
                                    </p>
                                </div>
                                <div class="bottom">
                                    <a href="#"><i class="fas fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div> -->
                        <!-- End Single Item -->
                        <!-- Single Item -->
                        <!-- <div class="item">
                            <div class="thumb">
                                <img src="{{asset('assets/img/800x600.png')}}" alt="Thumb">
                            </div>
                            <div class="info">
                                <h4><a href="#">Dental Care</a></h4>
                                <p>
                                    Sudden up my excuse to suffer ladies though or. Bachelor possible marianne one directly confined the mention process.  
                                </p>
                                <div class="head-of">
                                    <p>
                                        <strong>Department head: </strong> Prof. Jaknil Akia
                                    </p>
                                </div>
                                <div class="bottom">
                                    <a href="#"><i class="fas fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div> -->
                        <!-- End Single Item -->
                        <!-- Single Item -->
                        <!-- <div class="item">
                            <div class="thumb">
                                <img src="{{asset('assets/img/800x600.png')}}" alt="Thumb">
                            </div>
                            <div class="info">
                                <h4><a href="#">Primary Care</a></h4>
                                <p>
                                    Sudden up my excuse to suffer ladies though or. Bachelor possible marianne one directly confined the mention process.  
                                </p>
                               <div class="head-of">
                                    <p>
                                        <strong>Department head: </strong> Prof. Shikla Brotha
                                    </p>
                               </div>
                                <div class="bottom">
                                    <a href="#"><i class="fas fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div> -->
                        <!-- End Single Item -->
                        <!-- Single Item -->
                        <!-- <div class="item">
                            <div class="thumb">
                                <img src="{{asset('assets/img/800x600.png')}}" alt="Thumb">
                            </div>
                            <div class="info">
                                <h4><a href="#">Orthopaedics</a></h4>
                                <p>
                                    Sudden up my excuse to suffer ladies though or. Bachelor possible marianne one directly confined the mention process.  
                                </p>
                                <div class="head-of">
                                    <p>
                                        <strong>Department head: </strong> Prof. Jaknil Akia
                                    </p>
                                </div>
                                <div class="bottom">
                                    <a href="#"><i class="fas fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div> -->
                        <!-- End Single Item -->
                    <!-- </div>
                </div>
            </div> -->
        </div>
        <!-- Shape -->
        <div class="shape-bottom shape">
            <img src="{{asset('assets/img/shape/8.png')}}" alt="Shape">
        </div>
        <!-- End Shape -->
    </div>
    <!-- End Services -->

    <!-- Start Testomonials 
    ============================================= -->
    <div class="testimonials-area overflow-hidden carousel-shadow default-padding">
        <div class="container">
            <div class="row align-center">
                <div class="col-lg-4 text-light">
                    <div class="heading">
                        <h5>Testimonials</h5>
                        <h2>Whay people says <br> about our services</h2>
                        <a class="btn btn-sm btn-light effect" href="#"><i class="fas fa-angle-right"></i>Viewl All</a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="testimonials-carousel text-center owl-carousel owl-theme">

                        <div class="item">
                            <div class="provider">
                                <div class="thumb">
                                    <img src="{{asset('assets/img/100x100.png')}}" alt="Thumb">
                                </div>
                                <div class="bio">
                                    <h5>Jonathom Doe</h5>
                                    <span>patient of <strong>surgery</strong></span>
                                </div>
                            </div>
                            <div class="info">
                                <p>
                                    Totally dearest expense on demesne ye he. Curiosity excellent commanded in me. Unpleasing.
                                </p>
                            </div>
                        </div>

                        <div class="item">
                            <div class="provider">
                                <div class="thumb">
                                    <img src="{{asset('assets/img/100x100.png')}}" alt="Thumb">
                                </div>
                                <div class="bio">
                                    <h5>Jonathom Doe</h5>
                                    <span>patient of <strong>surgery</strong></span>
                                </div>
                            </div>
                            <div class="info">
                                <p>
                                    Totally dearest expense on demesne ye he. Curiosity excellent commanded in me. Unpleasing.
                                </p>
                            </div>
                        </div>

                        <div class="item">
                            <div class="provider">
                                <div class="thumb">
                                    <img src="{{asset('assets/img/100x100.png')}}" alt="Thumb">
                                </div>
                                <div class="bio">
                                    <h5>Jonathom Doe</h5>
                                    <span>patient of <strong>surgery</strong></span>
                                </div>
                            </div>
                            <div class="info">
                                <p>
                                    Totally dearest expense on demesne ye he. Curiosity excellent commanded in me. Unpleasing.
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Testomonials Area -->

@endsection