@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Form Metode Pembayaran</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('metodepembayaran') }}">Metode Pembayaran</a>
                                    </li>
                                    <li class="breadcrumb-item active">Edit Data Metode Pembayaran
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Validation -->
                <section class="bs-validation">
                    <div class="row">
                        <!-- Bootstrap Validation -->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Edit Data Metode Pembayaran</h4>
                                </div>
                                <div class="card-body">
                                    <form class="needs-validation" novalidate method="post" action="{{url('/metodepembayaran/'.$dmp->id.'/update')}}" enctype="multipart/form-data">
                                        
                                        @csrf
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Nama Pemilik Rekening</label>
                                            <input type="text" name="nama_pemilik_rekening" value="{{ old('nama_pemilik_rekening',$dmp->nama_pemilik_rekening) }}" id="basic-addon-name" class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Please enter your description.</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Metode Pembayaran</label>
                                            <input type="text" name="metode_pembayaran" value="{{ old('metode_pembayaran',$dmp->metode_pembayaran) }}" id="basic-addon-name" class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Please enter your description.</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Nama Provider</label>
                                            <input type="text" name="nama_provider" value="{{ old('nama_provider',$dmp->nama_provider) }}" id="basic-addon-name" class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Please enter your description.</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Foto Provider</label>
                                            <br>
                                            <img src="{{ asset('app-assets/images/pages/eCommerce/'.$dmp->foto_provider) }}" id ="foto_provider" alt="{{ $dmp->nama_provider}}" style="width: 5cm;">
                                        </div>
                                        <div class="mb-1">
                                            <input class="form-control" type="file" id="foto_provider" name="foto_provider">
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Nomor Rekening</label>
                                            <input type="text" name="no_rekening" value="{{ old('no_rekening',$dmp->no_rekening) }}" id="basic-addon-name" class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Please enter your description.</div>
                                        </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /Bootstrap Validation -->

                    </div>
                </section>
                <!-- /Validation -->

            </div>
        </div>
    </div>
@endsection