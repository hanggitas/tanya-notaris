@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-start mb-0">Form Order</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{ route('order') }}">Order</a>
                                    </li>
                                    <li class="breadcrumb-item active">Edit Data Order
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                    <div class="mb-1 breadcrumb-right">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                            <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                        </div>
                    </div>
                </div> --}}
            </div>
            <div class="content-body">
                <!-- Validation -->
                <section class="bs-validation">
                    <div class="row">
                        <!-- Bootstrap Validation class="needs-validation" novalidate-->
                        <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Edit Data Order</h4>
                                </div>
                                <div class="card-body">
                                    <form  method="post" action="{{ url('/order/'.$order->id) }}" enctype="multipart/form-data">
                                        
                                        @csrf
                                        @if($order->tipe_order == "reguler")
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Tipe Order</label>
                                            <input type="text" name="tipe_order" value="{{ old('tipe_order',$order->tipe_order) }}" id="basic-addon-name" class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" required disabled />
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Please enter your id user.</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Nama User</label>
                                            <select class="select2 form-select" id="select2-basic" name="id_users" disabled>
                                                <option value="">-- Pilih User --</option>
                                                @foreach($pilihan_user as $pilih_user)
                                                
                                                @if ($order->id_users == $pilih_user->id)
                                                <option value="{{ $pilih_user->id }}" selected>
                                                    {{ $pilih_user->name }}
                                                </option>
                                                @else
                                                <option value="{{ $pilih_user->id}}">
                                                    {{ $pilih_user->name }}
                                                </option>
                                                @endif
                                                
                                                @endforeach
                                            </select>
                                            {{-- <input type="text" name="name" value="{{ $pilihan_user->name }}" id="basic-addon-name" class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" required disabled/> --}}
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Please enter your id user.</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Metode Pembayaran</label>
                                            <select class="select2 form-select" id="select2-basic" name="id_metode_pembayaran">
                                                <option value="">-- Pilih Metode Pembayaran --</option>
                                                @foreach($pilihan_medpem as $pilih_medpem)
                                                
                                                @if ($order->id_metode_pembayaran == $pilih_medpem->id)
                                                <option value="{{ $pilih_medpem->id }}" selected>
                                                    {{ $pilih_medpem->metode_pembayaran }}
                                                </option>
                                                @else
                                                <option value="{{ $pilih_medpem->id}}">
                                                    {{ $pilih_medpem->metode_pembayaran }}
                                                </option>
                                                @endif
                                                
                                                @endforeach
                                            </select>
                                            {{-- <input type="text" name="id_metode_pembayaran" value="{{ old('id_metode_pembayaran',$order->metode_pembayaran) }}" id="basic-addon-name" class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" required /> --}}
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Please enter your id user.</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Status Pembayaran</label>
                                            <input type="text" name="status_pembayaran" value="{{ old('status_pembayaran',$order->status_pembayaran) }}" id="basic-addon-name" class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Please enter your id user.</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Total Harga</label>
                                            <input type="number" name="total_harga" value="{{ old('total_harga',$order->total_harga) }}" class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Please enter your id user.</div>
                                        </div>
                                        {{-- <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Total Dibayar</label>
                                            <input type="number" name="total_dibayar" value="{{ old('total_dibayar',$order->total_dibayar) }}"  class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Please enter your id user.</div>
                                        </div> --}}
                                        @else
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Tipe Order</label>
                                            <input type="text" name="tipe_order" value="{{ old('tipe_order',$order->tipe_order) }}" id="basic-addon-name" class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" required disabled />
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Please enter your id user.</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Nama User</label>
                                            <select class="select2 form-select" id="select2-basic" name="id_users" disabled>
                                                <option value="">-- Pilih User --</option>
                                                @foreach($pilihan_user as $pilih_user)
                                                
                                                @if ($order->id_users == $pilih_user->id)
                                                <option value="{{ $pilih_user->id }}" selected>
                                                    {{ $pilih_user->name }}
                                                </option>
                                                @else
                                                <option value="{{ $pilih_user->id}}">
                                                    {{ $pilih_user->name }}
                                                </option>
                                                @endif
                                                
                                                @endforeach
                                            </select>
                                            {{-- <input type="text" name="name" value="{{ $pilihan_user->name }}" id="basic-addon-name" class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" required disabled/> --}}
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Please enter your id user.</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Metode Pembayaran</label>
                                            <select class="select2 form-select" id="select2-basic" name="id_metode_pembayaran">
                                                <option value="">-- Pilih Metode Pembayaran --</option>
                                                @foreach($pilihan_medpem as $pilih_medpem)
                                                
                                                @if ($order->id_metode_pembayaran == $pilih_medpem->id)
                                                <option value="{{ $pilih_medpem->id }}" selected>
                                                    {{ $pilih_medpem->metode_pembayaran }}
                                                </option>
                                                @else
                                                <option value="{{ $pilih_medpem->id}}">
                                                    {{ $pilih_medpem->metode_pembayaran }}
                                                </option>
                                                @endif
                                                
                                                @endforeach
                                            </select>
                                            {{-- <input type="text" name="id_metode_pembayaran" value="{{ old('id_metode_pembayaran',$order->metode_pembayaran) }}" id="basic-addon-name" class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" required /> --}}
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Please enter your id user.</div>
                                        </div>
                                        <div class="mb-1">
                                           
                                            <label class="form-label" for="basic-addon-name">Status Pembayaran</label>
                                            {{-- <input type="text" name="status_pembayaran" value="{{ old('status_pembayaran',$order->status_pembayaran) }}" id="basic-addon-name" class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" required /> --}}
                                            <div class="demo-inline-spacing">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="status_pembayaran" id="menunggu" value="Menunggu" {{ old('status_pembayaran',$order->status_pembayaran == "Menunggu")? "checked" : "" }} />
                                                    <label class="form-check-label" for="menunggu">Menunggu</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="status_pembayaran" id="dp" value="Belum Lunas" {{ old('status_pembayaran',$order->status_pembayaran == 'Belum Lunas') ? "checked" : ""}} />
                                                    <label class="form-check-label" for="dp">Belum Lunas</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="status_pembayaran" id="lunas" value="Lunas" {{ old('status_pembayaran',$order->status_pembayaran == 'Lunas') ? "checked" : ""}} />
                                                    <label class="form-check-label" for="inlineRadio2">Pelunasan</label>
                                                </div>
                                            </div>
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Please enter your id user.</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Total Harga</label>
                                            <input type="number" name="total_harga" value="{{ number_format($order->total_harga, 0, '', '.') }}" class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" required disabled/>
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Please enter your id user.</div>
                                        </div>
                                        {{-- <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Total Dibayar</label>
                                            <input type="number" name="total_dibayar" value="{{ old('total_dibayar',$order->total_dibayar) }}" id="totalDibayar" class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" required />
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Please enter your id user.</div>
                                        </div> --}}
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Total Bayar DP</label>
                                            <input type="number" name="total_bayar_dp" value="{{ number_format($order->total_bayar_dp, 0, '', '.') }}" id="totalBayarDp" class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" required disabled/>
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Please enter your id user.</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Total Bayar Lunas</label>
                                            @if($order->status_pembayaran == "Lunas")
                                                <input type="number" name="total_bayar_lunas" value="{{ number_format($order->total_harga, 0, '', '.') }}"  class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" />
                                            @else
                                                <input type="number" name="total_bayar_lunas" value="0"  class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" />
                                            @endif
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Please enter your id user.</div>
                                        </div>
                                        <div class="mb-1">
                                            <label class="form-label" for="basic-addon-name">Sisa Bayar</label>
                                            @if($order->status_pembayaran == "Belum Lunas")
                                                <input type="number" name="sisa_bayar" value="{{ number_format($order->total_bayar_dp, 0, '', '.') }}" id="sisa" class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" />
                                            @else
                                            <input type="number" name="sisa_bayar" value="0" id="sisa" class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" />
                                            @endif 
                                            <div class="valid-feedback">Looks good!</div>
                                            <div class="invalid-feedback">Please enter your id user.</div>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                        @endif
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /Bootstrap Validation -->

                    </div>
                </section>
                <!-- /Validation -->

            </div>
        </div>
    </div> 
@endsection