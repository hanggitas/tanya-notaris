@extends('main.dashboard')
@extends('main.header')
@extends('main.menu')
@extends('main.footer')
@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-12 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Form Pekerjaan</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="{{ route('pekerjaan') }}">Pekerjaan</a>
                                </li>
                                <li class="breadcrumb-item active">Tambah Data Pekerjaan
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Validation -->
            <section class="bs-validation">
                <div class="row">
                    <!-- Bootstrap Validation -->
                    <div class="col-md-6 col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Tambah Data Pekerjaan</h4>
                            </div>
                            <div class="card-body">
                                <form class="needs-validation" novalidate method="post" action="{{ route('data_pekerjaan') }}">
                                    @csrf
                                    <div class="col-md-6 mb-1">
                                        <label class="form-label" for="select2-basic">ID Order</label>

                                        <select class="select2 form-select" id="select2-basic" name="id_order">
                                            <option value="">-- Pilih ID Order --</option>
                                            @foreach($pilihan_order as $pilih_order)
                                            
                                            <option value="{{ $pilih_order->id }}">
                                                {{ $pilih_order->id_users }}
                                            </option>
                                            
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label" for="basic-addon-name">Pre-Produksi</label>
                                        <input type="text" name="pre_produksi" id="basic-addon-name" class="form-control" placeholder="Silahkan input angkat 1" aria-label="Name" aria-describedby="basic-addon-name" required />
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label" for="basic-addon-name">Produksi</label>
                                        <input type="text" name="produksi" id="basic-addon-name" class="form-control" placeholder="Silahkan input angkat 1" aria-label="Name" aria-describedby="basic-addon-name" required />
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label" for="basic-addon-name">Post-Produksi</label>
                                        <input type="text" name="post_produksi" id="basic-addon-name" class="form-control" placeholder="Silahkan input angkat 1" aria-label="Name" aria-describedby="basic-addon-name" required />
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label" for="basic-addon-name">Selesai</label>
                                        <input type="text" name="selesai" id="basic-addon-name" class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" required />
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label" for="basic-addon-name">Link Projek</label>
                                        <input type="text" name="link_project" id="basic-addon-name" class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" required />
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label" for="basic-addon-name">Size Projek</label>
                                        <input type="text" name="size_project" id="basic-addon-name" class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" required />
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label" for="basic-addon-name">Diunggah</label>
                                        <input type="date" name="diunggah" id="basic-addon-name" class="form-control" placeholder="" aria-label="Name" aria-describedby="basic-addon-name" required />
                                    </div>
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /Bootstrap Validation -->
                </div>
            </section>
            <!-- /Validation -->

        </div>
    </div>
</div>
@endsection