<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->bigInteger('id_user')->unsigned();
            $table->bigInteger('id_metode_pembayaran')->unsigned();
            $table->longText('bukti_bayar');
            $table->date('date_time');
            $table->bigInteger('total_harga');
            $table->bigInteger('total_dibayar');
            $table->bigInteger('sisa_bayar');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
